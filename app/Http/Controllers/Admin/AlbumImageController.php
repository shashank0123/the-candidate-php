<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\AlbumImage;
use App\Models\User;
use App\Models\Album;
use App\Http\Controllers\Controller;

class AlbumImageController extends Controller
{
    public function index()
    {
        $data = AlbumImage::paginate(20);
        return view('admin.AlbumImage.listAlbumImage', compact('data'));
    }

    public function add()
    {
        
$users = User::all();
$albums = Album::all();

        return view('admin.AlbumImage.addAlbumImage', compact('users','albums'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['album_id'] = $data['album_id'];
$saveData['name'] = $data['name'];
$saveData['title'] = $data['title'];
$saveData['description'] = $data['description'];
$saveData['status'] = $data['status'];
$saveData['private'] = $data['private'];

 if (request()->hasFile('image')) {
               $path = request()->file('image')->store(
                   'file', 'public'
               );

               $data['image'] = \Storage::disk('public')->url($path);

            }


        $AlbumImage = AlbumImage::create($saveData);

        // return response()->json(['success' => true, 'data' => $AlbumImage], 200);
        return redirect('/admin/album_image')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = AlbumImage::where('id', $id)->first();$users = User::all();
$albums = Album::all();
return view('admin.AlbumImage.addAlbumImage', compact('row', 'users','albums'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['album_id'] = $data['album_id'];
$saveData['name'] = $data['name'];
$saveData['title'] = $data['title'];
$saveData['description'] = $data['description'];
$saveData['status'] = $data['status'];
$saveData['private'] = $data['private'];

 if (request()->hasFile('image')) {
               $path = request()->file('image')->store(
                   'file', 'public'
               );

               $saveData['image'] = \Storage::disk('public')->url($path);

            }
        $row = AlbumImage::where('id', $id)->first();
        if ($row){
            $AlbumImage = AlbumImage::where('id', $id)->update($saveData);
        }
        return redirect('/admin/album_image')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = AlbumImage::where('id', $request->id)->delete();
        return redirect('/admin/album_image');

    }


    public function getData(){
        $data = AlbumImage::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
