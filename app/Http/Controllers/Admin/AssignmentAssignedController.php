<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\AssignmentAssigned;
use App\Models\User;
use App\Models\Assignment;
use App\Http\Controllers\Controller;

class AssignmentAssignedController extends Controller
{
    public function index()
    {
        $data = AssignmentAssigned::paginate(20);
        return view('admin.AssignmentAssigned.listAssignmentAssigned', compact('data'));
    }

    public function add()
    {
        
$users = User::all();
$assignments = Assignment::all();

        return view('admin.AssignmentAssigned.addAssignmentAssigned', compact('users','assignments'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['assignment_id'] = $data['assignment_id'];
$saveData['price'] = $data['price'];
$saveData['start_date'] = $data['start_date'];
$saveData['timeline'] = $data['timeline'];
$saveData['role'] = $data['role'];


        $AssignmentAssigned = AssignmentAssigned::create($saveData);

        // return response()->json(['success' => true, 'data' => $AssignmentAssigned], 200);
        return redirect('/admin/assignment_assigned')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = AssignmentAssigned::where('id', $id)->first();$users = User::all();
$assignments = Assignment::all();
return view('admin.AssignmentAssigned.addAssignmentAssigned', compact('row', 'users','assignments'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['assignment_id'] = $data['assignment_id'];
$saveData['price'] = $data['price'];
$saveData['start_date'] = $data['start_date'];
$saveData['timeline'] = $data['timeline'];
$saveData['role'] = $data['role'];

        $row = AssignmentAssigned::where('id', $id)->first();
        if ($row){
            $AssignmentAssigned = AssignmentAssigned::where('id', $id)->update($saveData);
        }
        return redirect('/admin/assignment_assigned')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = AssignmentAssigned::where('id', $request->id)->delete();
        return redirect('/admin/assignment_assigned');

    }


    public function getData(){
        $data = AssignmentAssigned::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
