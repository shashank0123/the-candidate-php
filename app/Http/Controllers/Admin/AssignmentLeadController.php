<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\AssignmentLead;
use App\Models\Assignment;
use App\Models\User;
use App\Http\Controllers\Controller;

class AssignmentLeadController extends Controller
{
    public function index()
    {
        $data = AssignmentLead::paginate(20);
        return view('admin.AssignmentLead.listAssignmentLead', compact('data'));
    }

    public function add()
    {
        
$assignments = Assignment::all();
$users = User::all();

        return view('admin.AssignmentLead.addAssignmentLead', compact('assignments','users'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['assignment_id'] = $data['assignment_id'];
$saveData['lead_role'] = $data['lead_role'];
$saveData['user_id'] = $data['user_id'];
$saveData['status'] = $data['status'];


        $AssignmentLead = AssignmentLead::create($saveData);

        // return response()->json(['success' => true, 'data' => $AssignmentLead], 200);
        return redirect('/admin/assignment_lead')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = AssignmentLead::where('id', $id)->first();$assignments = Assignment::all();
$users = User::all();
return view('admin.AssignmentLead.addAssignmentLead', compact('row', 'assignments','users'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['assignment_id'] = $data['assignment_id'];
$saveData['lead_role'] = $data['lead_role'];
$saveData['user_id'] = $data['user_id'];
$saveData['status'] = $data['status'];

        $row = AssignmentLead::where('id', $id)->first();
        if ($row){
            $AssignmentLead = AssignmentLead::where('id', $id)->update($saveData);
        }
        return redirect('/admin/assignment_lead')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = AssignmentLead::where('id', $request->id)->delete();
        return redirect('/admin/assignment_lead');

    }


    public function getData(){
        $data = AssignmentLead::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
