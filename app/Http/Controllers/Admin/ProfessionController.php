<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Profession;
use App\Http\Controllers\Controller;

class ProfessionController extends Controller
{
    public function index()
    {
        $data = Profession::paginate(20);
        return view('admin.Profession.listProfession', compact('data'));
    }

    public function add()
    {
        

        return view('admin.Profession.addProfession');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['profession_name'] = $data['profession_name'];
$saveData['profession_slug'] = $data['profession_slug'];
$saveData['status'] = $data['status'];


        $Profession = Profession::create($saveData);

        // return response()->json(['success' => true, 'data' => $Profession], 200);
        return redirect('/admin/profession')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Profession::where('id', $id)->first();return view('admin.Profession.addProfession', compact('row' ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['profession_name'] = $data['profession_name'];
$saveData['profession_slug'] = $data['profession_slug'];
$saveData['status'] = $data['status'];

        $row = Profession::where('id', $id)->first();
        if ($row){
            $Profession = Profession::where('id', $id)->update($saveData);
        }
        return redirect('/admin/profession')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Profession::where('id', $request->id)->delete();
        return redirect('/admin/profession');

    }


    public function getData(){
        $data = Profession::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
