<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\User;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index()
    {
        $data = Profile::paginate(20);
        return view('admin.Profile.listProfile', compact('data'));
    }

    public function add()
    {
        
$users = User::all();

        return view('admin.Profile.addProfile', compact('users'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['profession'] = $data['profession'];
$saveData['dob'] = $data['dob'];
$saveData['looking_for'] = $data['looking_for'];
$saveData['is_freelancer'] = $data['is_freelancer'];
$saveData['career_start'] = $data['career_start'];


        $Profile = Profile::create($saveData);

        // return response()->json(['success' => true, 'data' => $Profile], 200);
        return redirect('/admin/profile')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Profile::where('id', $id)->first();$users = User::all();
return view('admin.Profile.addProfile', compact('row', 'users'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['profession'] = $data['profession'];
$saveData['dob'] = $data['dob'];
$saveData['looking_for'] = $data['looking_for'];
$saveData['is_freelancer'] = $data['is_freelancer'];
$saveData['career_start'] = $data['career_start'];

        $row = Profile::where('id', $id)->first();
        if ($row){
            $Profile = Profile::where('id', $id)->update($saveData);
        }
        return redirect('/admin/profile')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Profile::where('id', $request->id)->delete();
        return redirect('/admin/profile');

    }


    public function getData(){
        $data = Profile::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
