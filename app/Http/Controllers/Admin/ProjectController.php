<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index()
    {
        $data = Project::paginate(20);
        return view('admin.Project.listProject', compact('data'));
    }

    public function add()
    {
        

        return view('admin.Project.addProject');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['project_name'] = $data['project_name'];

 if (request()->hasFile('project_image')) {
               $path = request()->file('project_image')->store(
                   'file', 'public'
               );

               $data['project_image'] = \Storage::disk('public')->url($path);

            }
$saveData['description'] = $data['description'];
$saveData['start_date'] = $data['start_date'];
$saveData['project_type'] = $data['project_type'];
$saveData['status'] = $data['status'];


        $Project = Project::create($saveData);

        // return response()->json(['success' => true, 'data' => $Project], 200);
        return redirect('/admin/project')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Project::where('id', $id)->first();return view('admin.Project.addProject', compact('row' ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['project_name'] = $data['project_name'];

 if (request()->hasFile('project_image')) {
               $path = request()->file('project_image')->store(
                   'file', 'public'
               );

               $saveData['project_image'] = \Storage::disk('public')->url($path);

            }$saveData['description'] = $data['description'];
$saveData['start_date'] = $data['start_date'];
$saveData['project_type'] = $data['project_type'];
$saveData['status'] = $data['status'];

        $row = Project::where('id', $id)->first();
        if ($row){
            $Project = Project::where('id', $id)->update($saveData);
        }
        return redirect('/admin/project')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Project::where('id', $request->id)->delete();
        return redirect('/admin/project');

    }


    public function getData(){
        $data = Project::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
