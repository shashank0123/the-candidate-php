<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProjectLead;
use App\Models\Project;
use App\Models\User;
use App\Http\Controllers\Controller;

class ProjectLeadController extends Controller
{
    public function index()
    {
        $data = ProjectLead::paginate(20);
        return view('admin.ProjectLead.listProjectLead', compact('data'));
    }

    public function add()
    {
        
$projects = Project::all();
$users = User::all();

        return view('admin.ProjectLead.addProjectLead', compact('projects','users'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['lead_role'] = $data['lead_role'];
$saveData['user_id'] = $data['user_id'];
$saveData['status'] = $data['status'];


        $ProjectLead = ProjectLead::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProjectLead], 200);
        return redirect('/admin/project_lead')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProjectLead::where('id', $id)->first();$projects = Project::all();
$users = User::all();
return view('admin.ProjectLead.addProjectLead', compact('row', 'projects','users'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['lead_role'] = $data['lead_role'];
$saveData['user_id'] = $data['user_id'];
$saveData['status'] = $data['status'];

        $row = ProjectLead::where('id', $id)->first();
        if ($row){
            $ProjectLead = ProjectLead::where('id', $id)->update($saveData);
        }
        return redirect('/admin/project_lead')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ProjectLead::where('id', $request->id)->delete();
        return redirect('/admin/project_lead');

    }


    public function getData(){
        $data = ProjectLead::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
