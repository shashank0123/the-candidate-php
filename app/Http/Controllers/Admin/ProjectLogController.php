<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProjectLog;
use App\Models\Project;
use App\Http\Controllers\Controller;

class ProjectLogController extends Controller
{
    public function index()
    {
        $data = ProjectLog::paginate(20);
        return view('admin.ProjectLog.listProjectLog', compact('data'));
    }

    public function add()
    {
        
$projects = Project::all();

        return view('admin.ProjectLog.addProjectLog', compact('projects'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['log_details'] = $data['log_details'];
$saveData['status'] = $data['status'];


        $ProjectLog = ProjectLog::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProjectLog], 200);
        return redirect('/admin/project_log')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProjectLog::where('id', $id)->first();$projects = Project::all();
return view('admin.ProjectLog.addProjectLog', compact('row', 'projects'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['log_details'] = $data['log_details'];
$saveData['status'] = $data['status'];

        $row = ProjectLog::where('id', $id)->first();
        if ($row){
            $ProjectLog = ProjectLog::where('id', $id)->update($saveData);
        }
        return redirect('/admin/project_log')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ProjectLog::where('id', $request->id)->delete();
        return redirect('/admin/project_log');

    }


    public function getData(){
        $data = ProjectLog::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
