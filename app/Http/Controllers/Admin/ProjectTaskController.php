<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProjectTask;
use App\Models\Project;
use App\Http\Controllers\Controller;

class ProjectTaskController extends Controller
{
    public function index()
    {
        $data = ProjectTask::paginate(20);
        return view('admin.ProjectTask.listProjectTask', compact('data'));
    }

    public function add()
    {
        
$projects = Project::all();

        return view('admin.ProjectTask.addProjectTask', compact('projects'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['task_name'] = $data['task_name'];
$saveData['start_date'] = $data['start_date'];
$saveData['end_date'] = $data['end_date'];
$saveData['assigned_to'] = $data['assigned_to'];
$saveData['status'] = $data['status'];


        $ProjectTask = ProjectTask::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProjectTask], 200);
        return redirect('/admin/project_task')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProjectTask::where('id', $id)->first();$projects = Project::all();
return view('admin.ProjectTask.addProjectTask', compact('row', 'projects'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['task_name'] = $data['task_name'];
$saveData['start_date'] = $data['start_date'];
$saveData['end_date'] = $data['end_date'];
$saveData['assigned_to'] = $data['assigned_to'];
$saveData['status'] = $data['status'];

        $row = ProjectTask::where('id', $id)->first();
        if ($row){
            $ProjectTask = ProjectTask::where('id', $id)->update($saveData);
        }
        return redirect('/admin/project_task')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ProjectTask::where('id', $request->id)->delete();
        return redirect('/admin/project_task');

    }


    public function getData(){
        $data = ProjectTask::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
