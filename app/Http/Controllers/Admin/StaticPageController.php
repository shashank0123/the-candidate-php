<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\StaticPage;
use App\Http\Controllers\Controller;

class StaticPageController extends Controller
{
    public function index()
    {
        $data = StaticPage::paginate(20);
        return view('admin.StaticPage.listStaticPage', compact('data'));
    }

    public function add()
    {
        

        return view('admin.StaticPage.addStaticPage');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['page_name'] = $data['page_name'];
$saveData['content'] = $data['content'];
$saveData['status'] = $data['status'];
$saveData['add_to_menu'] = $data['add_to_menu'];


        $StaticPage = StaticPage::create($saveData);

        // return response()->json(['success' => true, 'data' => $StaticPage], 200);
        return redirect('/admin/staticpage')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = StaticPage::where('id', $id)->first();return view('admin.StaticPage.addStaticPage', compact('row' ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['page_name'] = $data['page_name'];
$saveData['content'] = $data['content'];
$saveData['status'] = $data['status'];
$saveData['add_to_menu'] = $data['add_to_menu'];

        $row = StaticPage::where('id', $id)->first();
        if ($row){
            $StaticPage = StaticPage::where('id', $id)->update($saveData);
        }
        return redirect('/admin/staticpage')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = StaticPage::where('id', $request->id)->delete();
        return redirect('/admin/staticpage');

    }


    public function getData(){
        $data = StaticPage::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
