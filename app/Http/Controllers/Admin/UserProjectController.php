<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\UserProject;
use App\Models\User;
use App\Models\Project;
use App\Http\Controllers\Controller;

class UserProjectController extends Controller
{
    public function index()
    {
        $data = UserProject::paginate(20);
        return view('admin.UserProject.listUserProject', compact('data'));
    }

    public function add()
    {
        
$users = User::all();
$projects = Project::all();

        return view('admin.UserProject.addUserProject', compact('users','projects'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['project_id'] = $data['project_id'];
$saveData['role'] = $data['role'];
$saveData['price'] = $data['price'];
$saveData['hire_date'] = $data['hire_date'];
$saveData['status'] = $data['status'];


        $UserProject = UserProject::create($saveData);

        // return response()->json(['success' => true, 'data' => $UserProject], 200);
        return redirect('/admin/user_project')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = UserProject::where('id', $id)->first();$users = User::all();
$projects = Project::all();
return view('admin.UserProject.addUserProject', compact('row', 'users','projects'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['project_id'] = $data['project_id'];
$saveData['role'] = $data['role'];
$saveData['price'] = $data['price'];
$saveData['hire_date'] = $data['hire_date'];
$saveData['status'] = $data['status'];

        $row = UserProject::where('id', $id)->first();
        if ($row){
            $UserProject = UserProject::where('id', $id)->update($saveData);
        }
        return redirect('/admin/user_project')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = UserProject::where('id', $request->id)->delete();
        return redirect('/admin/user_project');

    }


    public function getData(){
        $data = UserProject::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
