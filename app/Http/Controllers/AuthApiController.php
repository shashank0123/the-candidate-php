<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTFactory;
use JWTAuth;
use Response;
use Tymon\JWTAuth\Exceptions\JWTException;


class AuthApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $credentials = $request->only('name', 'mobile',  'email', 'password');
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'mobile' => 'required|unique:users',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;
        $mobile = $request->mobile;
        $user_name = $request->email;
        $email_token = base64_encode($request->email);
        $user = User::create(['email' => $email, 'name' => $name, 'mobile' => $mobile, 'user_name' => $user_name, 'password' => Hash::make($password)]);
        
        return $this->login($request);
    }



    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $rules = [
            'email' => 'required',
            'password' => 'required',
            'user_type' => 'user',
        ];
        $validator = Validator::make([
            'email' => $request->email,
            'password' => $request->password
        ], $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> "Please validate before sending"]);
        }
        try {
            $thiserror = JWTAuth::attempt([
                'user_name' => $request->email,
                'password' => $request->password
            ]);

            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt([
                'user_name' => $request->email,
                'password' => $request->password
            ])) {
                return response()->json(['success' => false, 'error' => $thiserror], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to login, please try again.'], 500);
        }
        // all good so return the token
        $user = User::where('email', $request->email)->orWhere('user_name', $request->email)->select('name', 'email', 'mobile')->first();
        if ($request->device_token){
            $token1 = DeviceToken::where('device_token', $request->device_token)->where('user_id', $user->id)->first();
            if (!$token1){
                $token1 = new DeviceToken;
                $token1->user_id = $user->id;
                $token1->device_token = $request->device_token;
                $token1->mobile = $request->mobile;
                $token1->imei = $request->deviceId;
                $token1->mobile_name = $request->device_type;
                $token1->save();
                $user->mobile = $request->mobile;
                $user->app_login_status = date('Y-m-d h:m:s');
                $user->update();
            }
            // else{
            //     $token1->mobile = $request->mobile;
            //     $token1->imei = $request->deviceId;
            //     $token1->mobile_name = $request->device_type;
            //     $token->update();
            // }
            
        }
        if ($user->parent_user>0){
            $user->user_type = 'sub';
        }
        if ($user->account_status != 'deactive')
            return response()->json(['success' => true, 'data'=> [ 'token' => $token, 'user' => $user]]);
        else
            return response()->json(['success' => false,'account_status' => 'deactivate', 'error' => 'Your account is blocked.'], 200);
    }

    public function googleloginfun(Request $request)
    {
        $credentials = $request->only('name', 'mobile',  'email', 'provider_id');
        $user = User::where('provider_id', $request->provider_id)->first();
        $user = User::where('email', $request->email)->first();
        $password = 'password123';
        $provider = $request->provider;
        if ($user){
            $user->provider_id = $request->provider_id;
            $user->provider = $provider;
            $user->update();

        }
        else{
            $rules = [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
            ];
            $validator = Validator::make($credentials, $rules);
            if($validator->fails()) {
                return response()->json(['success'=> false, 'error'=> $validator->messages()]);
                if (!$token = JWTAuth::attempt([
                    'email' => $request->email,
                    'password' => $password
                ])) {
                    return response()->json(['success' => false, 'error' => 'We cant  an account with this credentials.'], 401);
                }
                else{
                    return response()->json(['success' => true, 'data'=> [ 'token' => $token]]);    

                }
                return response()->json(['success'=> false, 'error'=> $validator->messages()]);
            }
        }
        $user = User::where('email', $request->email)->where('provider_id', $request->provider_id)->first();
        if (!isset($user) || !$token = JWTAuth::fromUser($user)) {
            $name = $request->name;
            $email = $request->email;
            $password = 'password@123';
            $mobile = $request->mobile;
            $provider_id = $request->provider_id;
            $user = User::create(['email' => $email,'provider_id' => $provider_id,'provider' => $provider ,'name' => $name, 'mobile' => $mobile, 'password' => Hash::make($password)]);
            if (!$token = JWTAuth::attempt([
                'email' => $request->email,
                'password' => $password
            ])) {
                return response()->json(['success' => false, 'error' => 'We cant find an account with this credentials.'], 401);
            }
            else{
                return response()->json(['success' => true, 'data'=> [ 'token' => $token]]);
            }
            
            return response()->json(['success' => false, 'error' => 'We cant find an account with this credentials.'], 401);
        }        
        return response()->json(['success' => true, 'data'=> [ 'token' => $token]]);    
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->guard()->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => 60* 60
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }
}
