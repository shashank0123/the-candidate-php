<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTFactory;
use StdClass;
use JWTAuth;
use Response;
use App\Models\Profile;
use Tymon\JWTAuth\Exceptions\JWTException;


class ProfileController extends Controller
{
    public function userProfile(Request $request)
    {
        $id = Auth::guard()->user();
        $user = new StdClass;
        $user->name = $id->name;
        $user->profile_pic = "https://khojapp.com/images/user/15839336231571834925ceb24.jpg";
        $user->followers = 0;
        $user->following = 0;
        $user->fb_link = 'shashank0312';
        $user->insta_link = 'shashank0312';
        $user->my_albums = [];
        array_push($user->my_albums, array('name' => 'Best Trip', 'images' => ["https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg"]));
        array_push($user->my_albums, array('name' => 'Best Trip2', 'images' => ["https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg"]));
        array_push($user->my_albums, array('name' => 'Best Trip3', 'images' => ["https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg"]));
        array_push($user->my_albums, array('name' => 'Best Trip4', 'images' => ["https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg"]));

        $user->my_images = [];
        array_push($user->my_images, ["https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg", "https://khojapp.com/images/user/15839336231571834925ceb24.jpg"]);



        $profile = Profile::where('user_id', $id->id)->first();
        $user->basic_info = new StdClass;
        $user->basic_info->experience = "2 Years";
        $user->basic_info->name = Auth::guard()->user()->name;
        $user->basic_info->profession = $profile->profession;
        $user->basic_info->height = $profile->height;

        

        return response()->json(['success' => true, 'data'=> $user]);

    }

    public function saveProfile(Request $request)
    {
        $profile = new Profile;
        $profile->user_id = Auth::guard()->user()->id;
        $profile->profession            = $request->profession;
        $profile->dob                   = $request->dob;
        $profile->looking_for           = $request->looking_for;
        $profile->is_freelancer         = $request->is_freelancer;
        $profile->career_start          = $request->career_start;
        $profile->insertOrUpdate();

        return response()->json(['success' => true, 'data'=> $profile]);
    }

    public function submit_profile(Request $request)
    {
        $profile = new Profile;
        $profile->user_id = Auth::guard()->user()->id;
        $profile->profession            = $request->profession;
        $profile->dob                   = $request->dob;
        $profile->looking_for           = $request->looking_for;
        $profile->is_freelancer         = $request->is_freelancer;
        $profile->career_start          = $request->career_start;
        $profile->about_me          = $request->about_me;
        $profile->insertOrUpdate();

        return response()->json(['success' => true, 'data'=> $profile]);
    }


    public function upload_image(Request $request)
    {
        $profile = new Profile;
        $profile->user_id = Auth::guard()->user()->id;
        $profile->profession            = $request->profession;
        $profile->dob                   = $request->dob;
        $profile->looking_for           = $request->looking_for;
        $profile->is_freelancer         = $request->is_freelancer;
        $profile->career_start          = $request->career_start;
        $profile->insertOrUpdate();

        return response()->json(['success' => true, 'data'=> $profile]);
    }
}
