<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTFactory;
use StdClass;
use JWTAuth;
use Response;
use App\Models\Profession;
use Tymon\JWTAuth\Exceptions\JWTException;


class SupportDataController extends Controller
{
    public function profession_list(Request $request)
    {
        $profession_list = Profession::select('id', 'profession_name as name')->get();
        return response()->json(['success' => true, 'data'=> $profession_list]);
    }

    public function getTnC(Request $request)
    {
        return response()->json(['success' => true, 'data'=> 'I Agree to the terms and conditions of The Candidate. I am providing correct details as per my knowledge. If anything is found incorrect, My Account will be suspended.']);
    }
}
