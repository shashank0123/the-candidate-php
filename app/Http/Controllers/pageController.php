<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Trip;
use App\Models\TripCategory;
use App\Models\Blog;
use App\Models\SiteSetting;
use App\Models\TripIterinary;

class pageController extends Controller
{

    public function __construct()
    {
        // $settings = SiteSetting::orderBy('id', 'desc')->first();
    }
    public function home(Request $request)
    {
        $trips = Trip::orderBy('id', 'desc')->limit(6)->get();
        $tripcategory = TripCategory::orderBy('position', 'asc')->limit(6)->get();
        $blogs = Blog::orderBy('id', 'desc')->limit(4)->get();
        return view('index', compact('trips', 'tripcategory', 'blogs'));
    }
    public function privacypolicy(Request $request)
    {
        $trips = Trip::orderBy('id', 'desc')->limit(6)->get();
        $tripcategory = TripCategory::orderBy('position', 'asc')->limit(4)->get();
        $blogs = Blog::orderBy('id', 'desc')->limit(4)->get();
        $customisedtrip = Trip::where('trip_type', 3)->orderBy('id', 'desc')->get();
        return view('index', compact('trips', 'tripcategory', 'blogs'));
    }
    public function cancellation(Request $request)
    {
        $trips = Trip::orderBy('id', 'desc')->limit(6)->get();
        $tripcategory = TripCategory::orderBy('position', 'asc')->limit(4)->get();
        $blogs = Blog::orderBy('id', 'desc')->limit(4)->get();
        return view('index', compact('trips', 'tripcategory', 'blogs'));
    }
    public function termsandconditions(Request $request)
    {
        $trips = Trip::orderBy('id', 'desc')->limit(6)->get();
        $tripcategory = TripCategory::orderBy('position', 'asc')->limit(4)->get();
        $blogs = Blog::orderBy('id', 'desc')->limit(4)->get();
        return view('index', compact('trips', 'tripcategory', 'blogs'));
    }
    public function disclaimer(Request $request)
    {
        $trips = Trip::orderBy('id', 'desc')->limit(6)->get();
        $tripcategory = TripCategory::orderBy('position', 'asc')->limit(4)->get();
        $blogs = Blog::orderBy('id', 'desc')->limit(4)->get();
        return view('index', compact('trips', 'tripcategory', 'blogs'));
    }

    public function category($category)
    {
        $cat_array = explode('-', $category);
        $id = $cat_array[count($cat_array) - 1];
        $tripcategory = TripCategory::where('id', $id)->first();
        $trips = Trip::where('category_id', $id)->get();
        if ($tripcategory) {
            return view('category', compact('tripcategory', 'trips'));
        } else {
            return redirect('/');
        }
    }


    public function trip($category, $trip)
    {
        $trip_array = explode('-', $trip);
        $id = $trip_array[count($trip_array) - 1];
        $trip = Trip::where('id', $id)->first();
        if ($trip) {
            $tripiterinaries = TripIterinary::where('trip_id', $id)->get();

            return view('trip', compact('trip', 'tripiterinaries'));
        } else {
            return redirect('/');
        }
        # code...
    }
}
