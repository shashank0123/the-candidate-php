<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
protected $fillable = ["assignment_name","assignment_image","description","start_date","project_type","status"];
}