<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $fillable = ["user_id","profession","dob","looking_for","is_freelancer","career_start"];

	function insertOrUpdate()
	{
		$checkdata = Profile::where('user_id', $this->user_id)->first();
		if ($checkdata){
			$profile = Profile::where('id', $checkdata->id)->update($this->toArray());
		}
		else
			$this->save();
	}


}