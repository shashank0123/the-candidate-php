<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
protected $fillable = ["project_name","project_image","description","start_date","project_type","status"];
}