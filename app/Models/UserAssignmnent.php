<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserAssignmnent extends Model
{
protected $fillable = ["user_id","assignment_id","role","price","hire_date","status"];
}