<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserProject extends Model
{
protected $fillable = ["user_id","project_id","role","price","hire_date","status"];
}