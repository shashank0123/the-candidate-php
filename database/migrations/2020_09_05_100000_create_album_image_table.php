<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlbumImageTable extends Migration

{
    public function up()
    {
        Schema::create('album_images', function (Blueprint $table) {
            $table->id();
$table->string('user_id')->nullable();
$table->string('album_id')->nullable();
$table->string('name')->nullable();
$table->string('title')->nullable();
$table->string('description')->nullable();
$table->string('status')->nullable();
$table->string('private')->nullable();
$table->string('image')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('album_images');
    }
}
