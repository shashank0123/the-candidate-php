<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessionTable extends Migration

{
    public function up()
    {
        Schema::create('professions', function (Blueprint $table) {
            $table->id();
$table->string('profession_name')->nullable();
$table->string('profession_slug')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('professions');
    }
}
