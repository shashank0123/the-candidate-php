<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileTable extends Migration

{
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
$table->string('user_id')->nullable();
$table->string('profession')->nullable();
$table->string('dob')->nullable();
$table->string('looking_for')->nullable();
$table->string('is_freelancer')->nullable();
$table->string('career_start')->nullable();
$table->text('about_me')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
