<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectLeadTable extends Migration

{
    public function up()
    {
        Schema::create('project_leads', function (Blueprint $table) {
            $table->id();
$table->string('project_id')->nullable();
$table->string('lead_role')->nullable();
$table->string('user_id')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('project_leads');
    }
}
