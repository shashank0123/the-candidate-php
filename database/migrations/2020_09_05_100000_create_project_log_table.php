<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectLogTable extends Migration

{
    public function up()
    {
        Schema::create('project_logs', function (Blueprint $table) {
            $table->id();
$table->string('project_id')->nullable();
$table->string('log_details')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('project_logs');
    }
}
