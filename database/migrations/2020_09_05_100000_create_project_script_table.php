<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectScriptTable extends Migration

{
    public function up()
    {
        Schema::create('project_scripts', function (Blueprint $table) {
            $table->id();
$table->string('project_id')->nullable();
$table->string('script_name')->nullable();
$table->string('description')->nullable();
$table->string('status')->nullable();
$table->string('written_by')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('project_scripts');
    }
}
