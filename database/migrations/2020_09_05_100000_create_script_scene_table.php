<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScriptSceneTable extends Migration

{
    public function up()
    {
        Schema::create('script_scenes', function (Blueprint $table) {
            $table->id();
$table->string('script_id')->nullable();
$table->string('name')->nullable();
$table->string('title')->nullable();
$table->string('description')->nullable();
$table->string('status')->nullable();
$table->string('private')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('script_scenes');
    }
}
