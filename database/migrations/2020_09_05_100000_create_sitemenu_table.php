<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteMenuTable extends Migration

{
    public function up()
    {
        Schema::create('sitemenus', function (Blueprint $table) {
            $table->id();
$table->string('menu_name')->nullable();
$table->string('menu_slug')->nullable();
$table->string('position')->nullable();
$table->string('status')->nullable();
$table->string('parent_menu')->nullable();
$table->string('target')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('sitemenus');
    }
}
