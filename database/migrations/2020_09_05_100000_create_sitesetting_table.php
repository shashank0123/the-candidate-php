<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteSettingTable extends Migration

{
    public function up()
    {
        Schema::create('sitesettings', function (Blueprint $table) {
            $table->id();
$table->string('site_name')->nullable();
$table->string('logo')->nullable();
$table->string('email')->nullable();
$table->string('phone')->nullable();
$table->string('google_analytics')->nullable();
$table->string('facebook_link')->nullable();
$table->string('instagram_link')->nullable();
$table->string('twitter_link')->nullable();
$table->text('footer')->nullable();
$table->text('about_us')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('sitesettings');
    }
}
