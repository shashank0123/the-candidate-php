<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticPageTable extends Migration

{
    public function up()
    {
        Schema::create('staticpages', function (Blueprint $table) {
            $table->id();
$table->string('page_name')->nullable();
$table->text('content')->nullable();
$table->string('status')->nullable();
$table->string('add_to_menu')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('staticpages');
    }
}
