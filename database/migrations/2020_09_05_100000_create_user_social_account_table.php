<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSocialAccountTable extends Migration

{
    public function up()
    {
        Schema::create('user_social_accounts', function (Blueprint $table) {
            $table->id();
$table->string('user_id')->nullable();
$table->string('social_account_id')->nullable();
$table->string('username')->nullable();
$table->string('auth_key')->nullable();
$table->string('extra')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('user_social_accounts');
    }
}
