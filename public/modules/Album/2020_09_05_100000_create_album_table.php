<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlbumTable extends Migration

{
    public function up()
    {
        Schema::create('albums', function (Blueprint $table) {
            $table->id();
$table->string('user_id')->nullable();
$table->string('name')->nullable();
$table->string('title')->nullable();
$table->string('description')->nullable();
$table->string('status')->nullable();
$table->string('private')->nullable();
$table->string('image')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('albums');
    }
}
