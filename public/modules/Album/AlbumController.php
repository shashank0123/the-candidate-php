<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Album;
use App\Models\User;
use App\Http\Controllers\Controller;

class AlbumController extends Controller
{
    public function index()
    {
        $data = Album::paginate(20);
        return view('admin.Album.listAlbum', compact('data'));
    }

    public function add()
    {
        
$users = User::all();

        return view('admin.Album.addAlbum', compact('users'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['name'] = $data['name'];
$saveData['title'] = $data['title'];
$saveData['description'] = $data['description'];
$saveData['status'] = $data['status'];
$saveData['private'] = $data['private'];
$saveData['image'] = $data['image'];


        $Album = Album::create($saveData);

        // return response()->json(['success' => true, 'data' => $Album], 200);
        return redirect('/admin/album')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Album::where('id', $id)->first();$users = User::all();
return view('admin.Album.addAlbum', compact('row', 'users'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['name'] = $data['name'];
$saveData['title'] = $data['title'];
$saveData['description'] = $data['description'];
$saveData['status'] = $data['status'];
$saveData['private'] = $data['private'];
$saveData['image'] = $data['image'];

        $row = Album::where('id', $id)->first();
        if ($row){
            $Album = Album::where('id', $id)->update($saveData);
        }
        return redirect('/admin/album')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Album::where('id', $request->id)->delete();
        return redirect('/admin/album');

    }


    public function getData(){
        $data = Album::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
