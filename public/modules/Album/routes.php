Route::get('/album','Admin\AlbumController@index');
    Route::get('album/add','Admin\AlbumController@add');
    Route::post('album/add','Admin\AlbumController@store');
    Route::get('album/edit/{id}','Admin\AlbumController@edit');
    Route::post('album/edit/{id}','Admin\AlbumController@update');
    Route::get('album/delete/{id}','Admin\AlbumController@delete');