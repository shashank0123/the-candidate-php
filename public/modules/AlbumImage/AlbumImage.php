<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AlbumImage extends Model
{
protected $fillable = ["user_id","album_id","name","title","description","status","private","image"];
}