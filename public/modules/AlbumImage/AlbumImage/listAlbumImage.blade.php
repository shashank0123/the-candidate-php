@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/album_image/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Album Image </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr><th>User Id</th><th>Album Id</th><th>Name</th><th>Title</th><th>Description</th><th>Status</th><th>Private</th><th>Image</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) 
                        <tr><td>{{$value->user_id}}</td><td>{{$value->album_id}}</td><td>{{$value->name}}</td><td>{{$value->title}}</td><td>{{$value->description}}</td><td>{{$value->status}}</td><td>{{$value->private}}</td><td>{{$value->image}}</td><td><a href='/admin/album_image/edit/{{$value->id}}' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/album_image/delete/{{$value->id}}' class='btn btn-danger' data-original-title='' title=''>delete</a></td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection