Route::get('/album_image','Admin\AlbumImageController@index');
    Route::get('album_image/add','Admin\AlbumImageController@add');
    Route::post('album_image/add','Admin\AlbumImageController@store');
    Route::get('album_image/edit/{id}','Admin\AlbumImageController@edit');
    Route::post('album_image/edit/{id}','Admin\AlbumImageController@update');
    Route::get('album_image/delete/{id}','Admin\AlbumImageController@delete');