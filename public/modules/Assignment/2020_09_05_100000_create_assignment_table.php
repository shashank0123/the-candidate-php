<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentTable extends Migration

{
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->id();
$table->string('assignment_name')->nullable();
$table->string('assignment_image')->nullable();
$table->string('description')->nullable();
$table->string('start_date')->nullable();
$table->string('project_type')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
