@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/assignment/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Assignment </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr><th>Assignment Name</th><th>Assignment Image</th><th>Description</th><th>Start Date</th><th>Project Type</th><th>Status</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) 
                        <tr><td>{{$value->assignment_name}}</td><td>{{$value->assignment_image}}</td><td>{{$value->description}}</td><td>{{$value->start_date}}</td><td>{{$value->project_type}}</td><td>{{$value->status}}</td><td><a href='/admin/assignment/edit/{{$value->id}}' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/assignment/delete/{{$value->id}}' class='btn btn-danger' data-original-title='' title=''>delete</a></td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection