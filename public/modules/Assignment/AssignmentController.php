<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Assignment;
use App\Http\Controllers\Controller;

class AssignmentController extends Controller
{
    public function index()
    {
        $data = Assignment::paginate(20);
        return view('admin.Assignment.listAssignment', compact('data'));
    }

    public function add()
    {
        

        return view('admin.Assignment.addAssignment');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['assignment_name'] = $data['assignment_name'];

 if (request()->hasFile('assignment_image')) {
               $path = request()->file('assignment_image')->store(
                   'file', 'public'
               );

               $data['assignment_image'] = \Storage::disk('public')->url($path);

            }
$saveData['description'] = $data['description'];
$saveData['start_date'] = $data['start_date'];
$saveData['project_type'] = $data['project_type'];
$saveData['status'] = $data['status'];


        $Assignment = Assignment::create($saveData);

        // return response()->json(['success' => true, 'data' => $Assignment], 200);
        return redirect('/admin/assignment')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Assignment::where('id', $id)->first();return view('admin.Assignment.addAssignment', compact('row' ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['assignment_name'] = $data['assignment_name'];

 if (request()->hasFile('assignment_image')) {
               $path = request()->file('assignment_image')->store(
                   'file', 'public'
               );

               $saveData['assignment_image'] = \Storage::disk('public')->url($path);

            }$saveData['description'] = $data['description'];
$saveData['start_date'] = $data['start_date'];
$saveData['project_type'] = $data['project_type'];
$saveData['status'] = $data['status'];

        $row = Assignment::where('id', $id)->first();
        if ($row){
            $Assignment = Assignment::where('id', $id)->update($saveData);
        }
        return redirect('/admin/assignment')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Assignment::where('id', $request->id)->delete();
        return redirect('/admin/assignment');

    }


    public function getData(){
        $data = Assignment::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
