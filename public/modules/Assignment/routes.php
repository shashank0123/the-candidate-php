Route::get('/assignment','Admin\AssignmentController@index');
    Route::get('assignment/add','Admin\AssignmentController@add');
    Route::post('assignment/add','Admin\AssignmentController@store');
    Route::get('assignment/edit/{id}','Admin\AssignmentController@edit');
    Route::post('assignment/edit/{id}','Admin\AssignmentController@update');
    Route::get('assignment/delete/{id}','Admin\AssignmentController@delete');