<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentAssignedTable extends Migration

{
    public function up()
    {
        Schema::create('assignment_assigneds', function (Blueprint $table) {
            $table->id();
$table->string('user_id')->nullable();
$table->string('assignment_id')->nullable();
$table->string('price')->nullable();
$table->string('start_date')->nullable();
$table->string('timeline')->nullable();
$table->string('role')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('assignment_assigneds');
    }
}
