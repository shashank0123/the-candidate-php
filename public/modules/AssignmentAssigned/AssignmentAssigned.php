<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AssignmentAssigned extends Model
{
protected $fillable = ["user_id","assignment_id","price","start_date","timeline","role"];
}