@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>User</label>
            <select class='form-control btn-square' name='user_id' id='user_id'>
                    <option value=''>Select</option>
                @foreach ($users as $item)
                    <option @if (isset($row->user_id) && $row->user_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Assignment</label>
            <select class='form-control btn-square' name='assignment_id' id='assignment_id'>
                    <option value=''>Select</option>
                @foreach ($assignments as $item)
                    <option @if (isset($row->assignment_id) && $row->assignment_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->assignment_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Price</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->price)){{$row->price}} @endif' name='price' placeholder='Enter Price'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Start Date</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->start_date)){{$row->start_date}} @endif' name='start_date' placeholder='Enter Start Date'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Timeline</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->timeline)){{$row->timeline}} @endif' name='timeline' placeholder='Enter Timeline'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Role</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->role)){{$row->role}} @endif' name='role' placeholder='Enter Role'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/assignment_assigned' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection