Route::get('/assignment_assigned','Admin\AssignmentAssignedController@index');
    Route::get('assignment_assigned/add','Admin\AssignmentAssignedController@add');
    Route::post('assignment_assigned/add','Admin\AssignmentAssignedController@store');
    Route::get('assignment_assigned/edit/{id}','Admin\AssignmentAssignedController@edit');
    Route::post('assignment_assigned/edit/{id}','Admin\AssignmentAssignedController@update');
    Route::get('assignment_assigned/delete/{id}','Admin\AssignmentAssignedController@delete');