<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentLeadTable extends Migration

{
    public function up()
    {
        Schema::create('assignment_leads', function (Blueprint $table) {
            $table->id();
$table->string('assignment_id')->nullable();
$table->string('lead_role')->nullable();
$table->string('user_id')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('assignment_leads');
    }
}
