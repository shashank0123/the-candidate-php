<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AssignmentLead extends Model
{
protected $fillable = ["assignment_id","lead_role","user_id","status"];
}