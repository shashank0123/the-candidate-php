Route::get('/assignment_lead','Admin\AssignmentLeadController@index');
    Route::get('assignment_lead/add','Admin\AssignmentLeadController@add');
    Route::post('assignment_lead/add','Admin\AssignmentLeadController@store');
    Route::get('assignment_lead/edit/{id}','Admin\AssignmentLeadController@edit');
    Route::post('assignment_lead/edit/{id}','Admin\AssignmentLeadController@update');
    Route::get('assignment_lead/delete/{id}','Admin\AssignmentLeadController@delete');