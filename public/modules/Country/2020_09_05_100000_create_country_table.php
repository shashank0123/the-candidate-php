<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryTable extends Migration

{
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
$table->string('country_name')->nullable();
$table->string('code')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
