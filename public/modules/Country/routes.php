Route::get('/country','Admin\CountryController@index');
    Route::get('country/add','Admin\CountryController@add');
    Route::post('country/add','Admin\CountryController@store');
    Route::get('country/edit/{id}','Admin\CountryController@edit');
    Route::post('country/edit/{id}','Admin\CountryController@update');
    Route::get('country/delete/{id}','Admin\CountryController@delete');