<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
protected $fillable = ["profession_name","profession_slug","status"];
}