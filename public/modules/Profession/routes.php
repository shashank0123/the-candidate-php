Route::get('/profession','Admin\ProfessionController@index');
    Route::get('profession/add','Admin\ProfessionController@add');
    Route::post('profession/add','Admin\ProfessionController@store');
    Route::get('profession/edit/{id}','Admin\ProfessionController@edit');
    Route::post('profession/edit/{id}','Admin\ProfessionController@update');
    Route::get('profession/delete/{id}','Admin\ProfessionController@delete');