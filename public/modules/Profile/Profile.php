<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
protected $fillable = ["user_id","profession","dob","looking_for","is_freelancer","career_start"];
}