@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>User</label>
            <select class='form-control btn-square' name='user_id' id='user_id'>
                    <option value=''>Select</option>
                @foreach ($users as $item)
                    <option @if (isset($row->user_id) && $row->user_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Profession</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->profession)){{$row->profession}} @endif' name='profession' placeholder='Enter Profession'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Dob</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->dob)){{$row->dob}} @endif' name='dob' placeholder='Enter Dob'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Looking For</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->looking_for)){{$row->looking_for}} @endif' name='looking_for' placeholder='Enter Looking For'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Is Freelancer</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->is_freelancer)){{$row->is_freelancer}} @endif' name='is_freelancer' placeholder='Enter Is Freelancer'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Career Start</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->career_start)){{$row->career_start}} @endif' name='career_start' placeholder='Enter Career Start'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/profile' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection