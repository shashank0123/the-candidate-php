Route::get('/profile','Admin\ProfileController@index');
    Route::get('profile/add','Admin\ProfileController@add');
    Route::post('profile/add','Admin\ProfileController@store');
    Route::get('profile/edit/{id}','Admin\ProfileController@edit');
    Route::post('profile/edit/{id}','Admin\ProfileController@update');
    Route::get('profile/delete/{id}','Admin\ProfileController@delete');