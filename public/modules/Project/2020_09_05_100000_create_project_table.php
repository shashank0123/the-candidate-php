<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectTable extends Migration

{
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
$table->string('project_name')->nullable();
$table->string('project_image')->nullable();
$table->string('description')->nullable();
$table->string('start_date')->nullable();
$table->string('project_type')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
