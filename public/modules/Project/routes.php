Route::get('/project','Admin\ProjectController@index');
    Route::get('project/add','Admin\ProjectController@add');
    Route::post('project/add','Admin\ProjectController@store');
    Route::get('project/edit/{id}','Admin\ProjectController@edit');
    Route::post('project/edit/{id}','Admin\ProjectController@update');
    Route::get('project/delete/{id}','Admin\ProjectController@delete');