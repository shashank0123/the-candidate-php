<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectCrewTable extends Migration

{
    public function up()
    {
        Schema::create('project_crews', function (Blueprint $table) {
            $table->id();
$table->string('project_id')->nullable();
$table->string('crew_role')->nullable();
$table->string('user_assigned')->nullable();
$table->string('user_title')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('project_crews');
    }
}
