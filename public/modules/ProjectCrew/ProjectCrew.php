<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectCrew extends Model
{
protected $fillable = ["project_id","crew_role","user_assigned","user_title","status"];
}