@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Project</label>
            <select class='form-control btn-square' name='project_id' id='project_id'>
                    <option value=''>Select</option>
                @foreach ($projects as $item)
                    <option @if (isset($row->project_id) && $row->project_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->project_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Crew Role</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->crew_role)){{$row->crew_role}} @endif' name='crew_role' placeholder='Enter Crew Role'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>User</label>
            <select class='form-control btn-square' name='user_assigned' id='user_assigned'>
                    <option value=''>Select</option>
                @foreach ($users as $item)
                    <option @if (isset($row->user_assigned) && $row->user_assigned == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>User Title</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->user_title)){{$row->user_title}} @endif' name='user_title' placeholder='Enter User Title'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Status</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->status)){{$row->status}} @endif' name='status' placeholder='Enter Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/project_crew' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection