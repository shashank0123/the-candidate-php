<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProjectCrew;
use App\Models\Project;
use App\Models\User;
use App\Http\Controllers\Controller;

class ProjectCrewController extends Controller
{
    public function index()
    {
        $data = ProjectCrew::paginate(20);
        return view('admin.ProjectCrew.listProjectCrew', compact('data'));
    }

    public function add()
    {
        
$projects = Project::all();
$users = User::all();

        return view('admin.ProjectCrew.addProjectCrew', compact('projects','users'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['crew_role'] = $data['crew_role'];
$saveData['user_assigned'] = $data['user_assigned'];
$saveData['user_title'] = $data['user_title'];
$saveData['status'] = $data['status'];


        $ProjectCrew = ProjectCrew::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProjectCrew], 200);
        return redirect('/admin/project_crew')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProjectCrew::where('id', $id)->first();$projects = Project::all();
$users = User::all();
return view('admin.ProjectCrew.addProjectCrew', compact('row', 'projects','users'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['crew_role'] = $data['crew_role'];
$saveData['user_assigned'] = $data['user_assigned'];
$saveData['user_title'] = $data['user_title'];
$saveData['status'] = $data['status'];

        $row = ProjectCrew::where('id', $id)->first();
        if ($row){
            $ProjectCrew = ProjectCrew::where('id', $id)->update($saveData);
        }
        return redirect('/admin/project_crew')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ProjectCrew::where('id', $request->id)->delete();
        return redirect('/admin/project_crew');

    }


    public function getData(){
        $data = ProjectCrew::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
