Route::get('/project_crew','Admin\ProjectCrewController@index');
    Route::get('project_crew/add','Admin\ProjectCrewController@add');
    Route::post('project_crew/add','Admin\ProjectCrewController@store');
    Route::get('project_crew/edit/{id}','Admin\ProjectCrewController@edit');
    Route::post('project_crew/edit/{id}','Admin\ProjectCrewController@update');
    Route::get('project_crew/delete/{id}','Admin\ProjectCrewController@delete');