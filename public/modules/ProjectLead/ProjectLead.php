<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectLead extends Model
{
protected $fillable = ["project_id","lead_role","user_id","status"];
}