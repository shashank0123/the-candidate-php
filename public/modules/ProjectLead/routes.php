Route::get('/project_lead','Admin\ProjectLeadController@index');
    Route::get('project_lead/add','Admin\ProjectLeadController@add');
    Route::post('project_lead/add','Admin\ProjectLeadController@store');
    Route::get('project_lead/edit/{id}','Admin\ProjectLeadController@edit');
    Route::post('project_lead/edit/{id}','Admin\ProjectLeadController@update');
    Route::get('project_lead/delete/{id}','Admin\ProjectLeadController@delete');