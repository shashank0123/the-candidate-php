Route::get('/project_log','Admin\ProjectLogController@index');
    Route::get('project_log/add','Admin\ProjectLogController@add');
    Route::post('project_log/add','Admin\ProjectLogController@store');
    Route::get('project_log/edit/{id}','Admin\ProjectLogController@edit');
    Route::post('project_log/edit/{id}','Admin\ProjectLogController@update');
    Route::get('project_log/delete/{id}','Admin\ProjectLogController@delete');