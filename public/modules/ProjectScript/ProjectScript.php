<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectScript extends Model
{
protected $fillable = ["project_id","script_name","description","status","written_by"];
}