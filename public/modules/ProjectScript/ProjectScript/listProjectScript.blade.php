@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/project_script/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Project Script </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr><th>Project Id</th><th>Script Name</th><th>Description</th><th>Status</th><th>Written By</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) 
                        <tr><td>{{$value->project_id}}</td><td>{{$value->script_name}}</td><td>{{$value->description}}</td><td>{{$value->status}}</td><td>{{$value->written_by}}</td><td><a href='/admin/project_script/edit/{{$value->id}}' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/project_script/delete/{{$value->id}}' class='btn btn-danger' data-original-title='' title=''>delete</a></td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection