<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProjectScript;
use App\Models\Project;
use App\Models\User;
use App\Http\Controllers\Controller;

class ProjectScriptController extends Controller
{
    public function index()
    {
        $data = ProjectScript::paginate(20);
        return view('admin.ProjectScript.listProjectScript', compact('data'));
    }

    public function add()
    {
        
$projects = Project::all();
$users = User::all();

        return view('admin.ProjectScript.addProjectScript', compact('projects','users'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['script_name'] = $data['script_name'];
$saveData['description'] = $data['description'];
$saveData['status'] = $data['status'];
$saveData['written_by'] = $data['written_by'];


        $ProjectScript = ProjectScript::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProjectScript], 200);
        return redirect('/admin/project_script')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProjectScript::where('id', $id)->first();$projects = Project::all();
$users = User::all();
return view('admin.ProjectScript.addProjectScript', compact('row', 'projects','users'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['script_name'] = $data['script_name'];
$saveData['description'] = $data['description'];
$saveData['status'] = $data['status'];
$saveData['written_by'] = $data['written_by'];

        $row = ProjectScript::where('id', $id)->first();
        if ($row){
            $ProjectScript = ProjectScript::where('id', $id)->update($saveData);
        }
        return redirect('/admin/project_script')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ProjectScript::where('id', $request->id)->delete();
        return redirect('/admin/project_script');

    }


    public function getData(){
        $data = ProjectScript::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
