Route::get('/project_script','Admin\ProjectScriptController@index');
    Route::get('project_script/add','Admin\ProjectScriptController@add');
    Route::post('project_script/add','Admin\ProjectScriptController@store');
    Route::get('project_script/edit/{id}','Admin\ProjectScriptController@edit');
    Route::post('project_script/edit/{id}','Admin\ProjectScriptController@update');
    Route::get('project_script/delete/{id}','Admin\ProjectScriptController@delete');