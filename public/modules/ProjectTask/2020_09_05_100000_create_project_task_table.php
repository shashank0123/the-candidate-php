<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectTaskTable extends Migration

{
    public function up()
    {
        Schema::create('project_tasks', function (Blueprint $table) {
            $table->id();
$table->string('project_id')->nullable();
$table->string('task_name')->nullable();
$table->string('start_date')->nullable();
$table->string('end_date')->nullable();
$table->string('assigned_to')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('project_tasks');
    }
}
