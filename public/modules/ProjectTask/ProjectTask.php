<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectTask extends Model
{
protected $fillable = ["project_id","task_name","start_date","end_date","assigned_to","status"];
}