@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Project</label>
            <select class='form-control btn-square' name='project_id' id='project_id'>
                    <option value=''>Select</option>
                @foreach ($projects as $item)
                    <option @if (isset($row->project_id) && $row->project_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->project_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Task Name</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->task_name)){{$row->task_name}} @endif' name='task_name' placeholder='Enter Task Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Start Date</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->start_date)){{$row->start_date}} @endif' name='start_date' placeholder='Enter Start Date'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>End Date</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->end_date)){{$row->end_date}} @endif' name='end_date' placeholder='Enter End Date'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Assigned To</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->assigned_to)){{$row->assigned_to}} @endif' name='assigned_to' placeholder='Enter Assigned To'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Status</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->status)){{$row->status}} @endif' name='status' placeholder='Enter Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/project_task' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection