Route::get('/project_task','Admin\ProjectTaskController@index');
    Route::get('project_task/add','Admin\ProjectTaskController@add');
    Route::post('project_task/add','Admin\ProjectTaskController@store');
    Route::get('project_task/edit/{id}','Admin\ProjectTaskController@edit');
    Route::post('project_task/edit/{id}','Admin\ProjectTaskController@update');
    Route::get('project_task/delete/{id}','Admin\ProjectTaskController@delete');