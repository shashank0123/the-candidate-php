<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ScriptScene extends Model
{
protected $fillable = ["script_id","name","title","description","status","private"];
}