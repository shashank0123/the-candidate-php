<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ScriptScene;
use App\Models\ProjectScript;
use App\Http\Controllers\Controller;

class ScriptSceneController extends Controller
{
    public function index()
    {
        $data = ScriptScene::paginate(20);
        return view('admin.ScriptScene.listScriptScene', compact('data'));
    }

    public function add()
    {
        
$projectscripts = ProjectScript::all();

        return view('admin.ScriptScene.addScriptScene', compact('projectscripts'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['script_id'] = $data['script_id'];
$saveData['name'] = $data['name'];
$saveData['title'] = $data['title'];
$saveData['description'] = $data['description'];
$saveData['status'] = $data['status'];
$saveData['private'] = $data['private'];


        $ScriptScene = ScriptScene::create($saveData);

        // return response()->json(['success' => true, 'data' => $ScriptScene], 200);
        return redirect('/admin/script_scene')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ScriptScene::where('id', $id)->first();$projectscripts = ProjectScript::all();
return view('admin.ScriptScene.addScriptScene', compact('row', 'projectscripts'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['script_id'] = $data['script_id'];
$saveData['name'] = $data['name'];
$saveData['title'] = $data['title'];
$saveData['description'] = $data['description'];
$saveData['status'] = $data['status'];
$saveData['private'] = $data['private'];

        $row = ScriptScene::where('id', $id)->first();
        if ($row){
            $ScriptScene = ScriptScene::where('id', $id)->update($saveData);
        }
        return redirect('/admin/script_scene')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ScriptScene::where('id', $request->id)->delete();
        return redirect('/admin/script_scene');

    }


    public function getData(){
        $data = ScriptScene::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
