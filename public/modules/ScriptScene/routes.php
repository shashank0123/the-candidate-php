Route::get('/script_scene','Admin\ScriptSceneController@index');
    Route::get('script_scene/add','Admin\ScriptSceneController@add');
    Route::post('script_scene/add','Admin\ScriptSceneController@store');
    Route::get('script_scene/edit/{id}','Admin\ScriptSceneController@edit');
    Route::post('script_scene/edit/{id}','Admin\ScriptSceneController@update');
    Route::get('script_scene/delete/{id}','Admin\ScriptSceneController@delete');