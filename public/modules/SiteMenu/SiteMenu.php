<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SiteMenu extends Model
{
protected $fillable = ["menu_name","menu_slug","position","status","parent_menu","target"];
}