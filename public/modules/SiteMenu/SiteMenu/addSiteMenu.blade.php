@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Menu Name</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->menu_name)){{$row->menu_name}} @endif' name='menu_name' placeholder='Enter Menu Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Menu Slug</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->menu_slug)){{$row->menu_slug}} @endif' name='menu_slug' placeholder='Enter Menu Slug'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Position</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->position)){{$row->position}} @endif' name='position' placeholder='Enter Position'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Status</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->status)){{$row->status}} @endif' name='status' placeholder='Enter Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>SiteMenu</label>
            <select class='form-control btn-square' name='parent_menu' id='parent_menu'>
                    <option value=''>Select</option>
                @foreach ($sitemenus as $item)
                    <option @if (isset($row->parent_menu) && $row->parent_menu == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->menu_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Target</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->target)){{$row->target}} @endif' name='target' placeholder='Enter Target'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/sitemenu' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection