<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\SiteMenu;
use App\Http\Controllers\Controller;

class SiteMenuController extends Controller
{
    public function index()
    {
        $data = SiteMenu::paginate(20);
        return view('admin.SiteMenu.listSiteMenu', compact('data'));
    }

    public function add()
    {
        
$sitemenus = SiteMenu::all();

        return view('admin.SiteMenu.addSiteMenu', compact('sitemenus'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['menu_name'] = $data['menu_name'];
$saveData['menu_slug'] = $data['menu_slug'];
$saveData['position'] = $data['position'];
$saveData['status'] = $data['status'];
$saveData['parent_menu'] = $data['parent_menu'];
$saveData['target'] = $data['target'];


        $SiteMenu = SiteMenu::create($saveData);

        // return response()->json(['success' => true, 'data' => $SiteMenu], 200);
        return redirect('/admin/sitemenu')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = SiteMenu::where('id', $id)->first();$sitemenus = SiteMenu::all();
return view('admin.SiteMenu.addSiteMenu', compact('row', 'sitemenus'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['menu_name'] = $data['menu_name'];
$saveData['menu_slug'] = $data['menu_slug'];
$saveData['position'] = $data['position'];
$saveData['status'] = $data['status'];
$saveData['parent_menu'] = $data['parent_menu'];
$saveData['target'] = $data['target'];

        $row = SiteMenu::where('id', $id)->first();
        if ($row){
            $SiteMenu = SiteMenu::where('id', $id)->update($saveData);
        }
        return redirect('/admin/sitemenu')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = SiteMenu::where('id', $request->id)->delete();
        return redirect('/admin/sitemenu');

    }


    public function getData(){
        $data = SiteMenu::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
