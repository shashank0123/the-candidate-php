Route::get('/sitemenu','Admin\SiteMenuController@index');
    Route::get('sitemenu/add','Admin\SiteMenuController@add');
    Route::post('sitemenu/add','Admin\SiteMenuController@store');
    Route::get('sitemenu/edit/{id}','Admin\SiteMenuController@edit');
    Route::post('sitemenu/edit/{id}','Admin\SiteMenuController@update');
    Route::get('sitemenu/delete/{id}','Admin\SiteMenuController@delete');