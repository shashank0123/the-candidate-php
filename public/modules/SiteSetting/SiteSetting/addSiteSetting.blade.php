@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Site Name</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->site_name)){{$row->site_name}} @endif' name='site_name' placeholder='Enter Site Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Logo</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->logo)){{$row->logo}} @endif' name='logo' placeholder='Enter Logo'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Email</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->email)){{$row->email}} @endif' name='email' placeholder='Enter Email'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Phone</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->phone)){{$row->phone}} @endif' name='phone' placeholder='Enter Phone'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Google Analytics</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->google_analytics)){{$row->google_analytics}} @endif' name='google_analytics' placeholder='Enter Google Analytics'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Facebook Link</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->facebook_link)){{$row->facebook_link}} @endif' name='facebook_link' placeholder='Enter Facebook Link'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Instagram Link</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->instagram_link)){{$row->instagram_link}} @endif' name='instagram_link' placeholder='Enter Instagram Link'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Twitter Link</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->twitter_link)){{$row->twitter_link}} @endif' name='twitter_link' placeholder='Enter Twitter Link'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='footer'>footer</label>
                    <div class='col-lg-12'>
                        <textarea name='footer' class='tinyMCE'>@if (isset($row->footer)){{$row->footer}} @endif</textarea>
                    </div>
                    </div><div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='about_us'>about us</label>
                    <div class='col-lg-12'>
                        <textarea name='about_us' class='tinyMCE'>@if (isset($row->about_us)){{$row->about_us}} @endif</textarea>
                    </div>
                    </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/sitesetting' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection