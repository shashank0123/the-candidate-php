@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/sitesetting/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>SiteSetting </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr><th>Site Name</th><th>Logo</th><th>Email</th><th>Phone</th><th>Google Analytics</th><th>Facebook Link</th><th>Instagram Link</th><th>Twitter Link</th><th>Footer</th><th>About Us</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) 
                        <tr><td>{{$value->site_name}}</td><td>{{$value->logo}}</td><td>{{$value->email}}</td><td>{{$value->phone}}</td><td>{{$value->google_analytics}}</td><td>{{$value->facebook_link}}</td><td>{{$value->instagram_link}}</td><td>{{$value->twitter_link}}</td><td>{{$value->footer}}</td><td>{{$value->about_us}}</td><td><a href='/admin/sitesetting/edit/{{$value->id}}' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/sitesetting/delete/{{$value->id}}' class='btn btn-danger' data-original-title='' title=''>delete</a></td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection