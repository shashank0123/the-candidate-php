<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\SiteSetting;
use App\Http\Controllers\Controller;

class SiteSettingController extends Controller
{
    public function index()
    {
        $data = SiteSetting::paginate(20);
        return view('admin.SiteSetting.listSiteSetting', compact('data'));
    }

    public function add()
    {
        

        return view('admin.SiteSetting.addSiteSetting');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['site_name'] = $data['site_name'];
$saveData['logo'] = $data['logo'];
$saveData['email'] = $data['email'];
$saveData['phone'] = $data['phone'];
$saveData['google_analytics'] = $data['google_analytics'];
$saveData['facebook_link'] = $data['facebook_link'];
$saveData['instagram_link'] = $data['instagram_link'];
$saveData['twitter_link'] = $data['twitter_link'];
$saveData['footer'] = $data['footer'];
$saveData['about_us'] = $data['about_us'];


        $SiteSetting = SiteSetting::create($saveData);

        // return response()->json(['success' => true, 'data' => $SiteSetting], 200);
        return redirect('/admin/sitesetting')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = SiteSetting::where('id', $id)->first();return view('admin.SiteSetting.addSiteSetting', compact('row' ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['site_name'] = $data['site_name'];
$saveData['logo'] = $data['logo'];
$saveData['email'] = $data['email'];
$saveData['phone'] = $data['phone'];
$saveData['google_analytics'] = $data['google_analytics'];
$saveData['facebook_link'] = $data['facebook_link'];
$saveData['instagram_link'] = $data['instagram_link'];
$saveData['twitter_link'] = $data['twitter_link'];
$saveData['footer'] = $data['footer'];
$saveData['about_us'] = $data['about_us'];

        $row = SiteSetting::where('id', $id)->first();
        if ($row){
            $SiteSetting = SiteSetting::where('id', $id)->update($saveData);
        }
        return redirect('/admin/sitesetting')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = SiteSetting::where('id', $request->id)->delete();
        return redirect('/admin/sitesetting');

    }


    public function getData(){
        $data = SiteSetting::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
