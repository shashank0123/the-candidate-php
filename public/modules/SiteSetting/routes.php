Route::get('/sitesetting','Admin\SiteSettingController@index');
    Route::get('sitesetting/add','Admin\SiteSettingController@add');
    Route::post('sitesetting/add','Admin\SiteSettingController@store');
    Route::get('sitesetting/edit/{id}','Admin\SiteSettingController@edit');
    Route::post('sitesetting/edit/{id}','Admin\SiteSettingController@update');
    Route::get('sitesetting/delete/{id}','Admin\SiteSettingController@delete');