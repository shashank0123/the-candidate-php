Route::get('/slider','Admin\SliderController@index');
    Route::get('slider/add','Admin\SliderController@add');
    Route::post('slider/add','Admin\SliderController@store');
    Route::get('slider/edit/{id}','Admin\SliderController@edit');
    Route::post('slider/edit/{id}','Admin\SliderController@update');
    Route::get('slider/delete/{id}','Admin\SliderController@delete');