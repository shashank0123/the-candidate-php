<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialAccountTable extends Migration

{
    public function up()
    {
        Schema::create('social_accounts', function (Blueprint $table) {
            $table->id();
$table->string('account_name')->nullable();
$table->string('base_url')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('social_accounts');
    }
}
