<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
protected $fillable = ["account_name","base_url","status"];
}