Route::get('/social_account','Admin\SocialAccountController@index');
    Route::get('social_account/add','Admin\SocialAccountController@add');
    Route::post('social_account/add','Admin\SocialAccountController@store');
    Route::get('social_account/edit/{id}','Admin\SocialAccountController@edit');
    Route::post('social_account/edit/{id}','Admin\SocialAccountController@update');
    Route::get('social_account/delete/{id}','Admin\SocialAccountController@delete');