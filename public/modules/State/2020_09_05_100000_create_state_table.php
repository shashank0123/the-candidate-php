<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStateTable extends Migration

{
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->id();
$table->string('country_id')->nullable();
$table->string('state_name')->nullable();
$table->string('state_code')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
