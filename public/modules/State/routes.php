Route::get('/state','Admin\StateController@index');
    Route::get('state/add','Admin\StateController@add');
    Route::post('state/add','Admin\StateController@store');
    Route::get('state/edit/{id}','Admin\StateController@edit');
    Route::post('state/edit/{id}','Admin\StateController@update');
    Route::get('state/delete/{id}','Admin\StateController@delete');