<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class StaticPage extends Model
{
protected $fillable = ["page_name","content","status","add_to_menu"];
}