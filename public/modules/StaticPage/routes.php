Route::get('/staticpage','Admin\StaticPageController@index');
    Route::get('staticpage/add','Admin\StaticPageController@add');
    Route::post('staticpage/add','Admin\StaticPageController@store');
    Route::get('staticpage/edit/{id}','Admin\StaticPageController@edit');
    Route::post('staticpage/edit/{id}','Admin\StaticPageController@update');
    Route::get('staticpage/delete/{id}','Admin\StaticPageController@delete');