<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAssignmnentTable extends Migration

{
    public function up()
    {
        Schema::create('user_assignmnents', function (Blueprint $table) {
            $table->id();
$table->string('user_id')->nullable();
$table->string('assignment_id')->nullable();
$table->string('role')->nullable();
$table->string('price')->nullable();
$table->string('hire_date')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('user_assignmnents');
    }
}
