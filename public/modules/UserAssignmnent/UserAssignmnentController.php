<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\UserAssignmnent;
use App\Models\User;
use App\Models\Assignment;
use App\Http\Controllers\Controller;

class UserAssignmnentController extends Controller
{
    public function index()
    {
        $data = UserAssignmnent::paginate(20);
        return view('admin.UserAssignmnent.listUserAssignmnent', compact('data'));
    }

    public function add()
    {
        
$users = User::all();
$assignments = Assignment::all();

        return view('admin.UserAssignmnent.addUserAssignmnent', compact('users','assignments'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['assignment_id'] = $data['assignment_id'];
$saveData['role'] = $data['role'];
$saveData['price'] = $data['price'];
$saveData['hire_date'] = $data['hire_date'];
$saveData['status'] = $data['status'];


        $UserAssignmnent = UserAssignmnent::create($saveData);

        // return response()->json(['success' => true, 'data' => $UserAssignmnent], 200);
        return redirect('/admin/user_assignmnent')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = UserAssignmnent::where('id', $id)->first();$users = User::all();
$assignments = Assignment::all();
return view('admin.UserAssignmnent.addUserAssignmnent', compact('row', 'users','assignments'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['assignment_id'] = $data['assignment_id'];
$saveData['role'] = $data['role'];
$saveData['price'] = $data['price'];
$saveData['hire_date'] = $data['hire_date'];
$saveData['status'] = $data['status'];

        $row = UserAssignmnent::where('id', $id)->first();
        if ($row){
            $UserAssignmnent = UserAssignmnent::where('id', $id)->update($saveData);
        }
        return redirect('/admin/user_assignmnent')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = UserAssignmnent::where('id', $request->id)->delete();
        return redirect('/admin/user_assignmnent');

    }


    public function getData(){
        $data = UserAssignmnent::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
