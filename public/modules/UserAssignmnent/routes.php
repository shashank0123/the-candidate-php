Route::get('/user_assignmnent','Admin\UserAssignmnentController@index');
    Route::get('user_assignmnent/add','Admin\UserAssignmnentController@add');
    Route::post('user_assignmnent/add','Admin\UserAssignmnentController@store');
    Route::get('user_assignmnent/edit/{id}','Admin\UserAssignmnentController@edit');
    Route::post('user_assignmnent/edit/{id}','Admin\UserAssignmnentController@update');
    Route::get('user_assignmnent/delete/{id}','Admin\UserAssignmnentController@delete');