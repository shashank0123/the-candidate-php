Route::get('/user_project','Admin\UserProjectController@index');
    Route::get('user_project/add','Admin\UserProjectController@add');
    Route::post('user_project/add','Admin\UserProjectController@store');
    Route::get('user_project/edit/{id}','Admin\UserProjectController@edit');
    Route::post('user_project/edit/{id}','Admin\UserProjectController@update');
    Route::get('user_project/delete/{id}','Admin\UserProjectController@delete');