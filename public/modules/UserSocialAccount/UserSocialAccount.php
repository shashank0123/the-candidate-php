<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserSocialAccount extends Model
{
protected $fillable = ["user_id","social_account_id","username","auth_key","extra","status"];
}