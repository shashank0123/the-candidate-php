<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\UserSocialAccount;
use App\Models\User;
use App\Models\SocialAccount;
use App\Http\Controllers\Controller;

class UserSocialAccountController extends Controller
{
    public function index()
    {
        $data = UserSocialAccount::paginate(20);
        return view('admin.UserSocialAccount.listUserSocialAccount', compact('data'));
    }

    public function add()
    {
        
$users = User::all();
$socialaccounts = SocialAccount::all();

        return view('admin.UserSocialAccount.addUserSocialAccount', compact('users','socialaccounts'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['social_account_id'] = $data['social_account_id'];
$saveData['username'] = $data['username'];
$saveData['auth_key'] = $data['auth_key'];
$saveData['extra'] = $data['extra'];
$saveData['status'] = $data['status'];


        $UserSocialAccount = UserSocialAccount::create($saveData);

        // return response()->json(['success' => true, 'data' => $UserSocialAccount], 200);
        return redirect('/admin/user_social_account')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = UserSocialAccount::where('id', $id)->first();$users = User::all();
$socialaccounts = SocialAccount::all();
return view('admin.UserSocialAccount.addUserSocialAccount', compact('row', 'users','socialaccounts'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['social_account_id'] = $data['social_account_id'];
$saveData['username'] = $data['username'];
$saveData['auth_key'] = $data['auth_key'];
$saveData['extra'] = $data['extra'];
$saveData['status'] = $data['status'];

        $row = UserSocialAccount::where('id', $id)->first();
        if ($row){
            $UserSocialAccount = UserSocialAccount::where('id', $id)->update($saveData);
        }
        return redirect('/admin/user_social_account')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = UserSocialAccount::where('id', $request->id)->delete();
        return redirect('/admin/user_social_account');

    }


    public function getData(){
        $data = UserSocialAccount::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
