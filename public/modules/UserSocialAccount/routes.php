Route::get('/user_social_account','Admin\UserSocialAccountController@index');
    Route::get('user_social_account/add','Admin\UserSocialAccountController@add');
    Route::post('user_social_account/add','Admin\UserSocialAccountController@store');
    Route::get('user_social_account/edit/{id}','Admin\UserSocialAccountController@edit');
    Route::post('user_social_account/edit/{id}','Admin\UserSocialAccountController@update');
    Route::get('user_social_account/delete/{id}','Admin\UserSocialAccountController@delete');