@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>User</label>
            <select class='form-control btn-square' name='user_id' id='user_id'>
                    <option value=''>Select</option>
                @foreach ($users as $item)
                    <option @if (isset($row->user_id) && $row->user_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Album</label>
            <select class='form-control btn-square' name='album_id' id='album_id'>
                    <option value=''>Select</option>
                @foreach ($albums as $item)
                    <option @if (isset($row->album_id) && $row->album_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Name</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->name)){{$row->name}} @endif' name='name' placeholder='Enter Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Title</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->title)){{$row->title}} @endif' name='title' placeholder='Enter Title'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Description</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->description)){{$row->description}} @endif' name='description' placeholder='Enter Description'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Status</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->status)){{$row->status}} @endif' name='status' placeholder='Enter Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Private</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->private)){{$row->private}} @endif' name='private' placeholder='Enter Private'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='image'>image</label>
                    @if (isset($row->image) && $row->image!='')<img width='200px' height='200px' src='{{$row->image}}'> @endif

                    <div class='col-lg-12'>
                        <input id='image' name='image' class='input-file' type='file'>
                    </div>
                    </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/album_image' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection