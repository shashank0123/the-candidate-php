@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Project Name</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->project_name)){{$row->project_name}} @endif' name='project_name' placeholder='Enter Project Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='project_image'>project image</label>
                    @if (isset($row->project_image) && $row->project_image!='')<img width='200px' height='200px' src='{{$row->project_image}}'> @endif

                    <div class='col-lg-12'>
                        <input id='project_image' name='project_image' class='input-file' type='file'>
                    </div>
                    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Description</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->description)){{$row->description}} @endif' name='description' placeholder='Enter Description'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Start Date</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->start_date)){{$row->start_date}} @endif' name='start_date' placeholder='Enter Start Date'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Project Type</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->project_type)){{$row->project_type}} @endif' name='project_type' placeholder='Enter Project Type'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Status</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->status)){{$row->status}} @endif' name='status' placeholder='Enter Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/project' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection