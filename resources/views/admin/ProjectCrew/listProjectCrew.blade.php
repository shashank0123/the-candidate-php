@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/project_crew/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Project Crew </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr><th>Project Id</th><th>Crew Role</th><th>User Assigned</th><th>User Title</th><th>Status</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) 
                        <tr><td>{{$value->project_id}}</td><td>{{$value->crew_role}}</td><td>{{$value->user_assigned}}</td><td>{{$value->user_title}}</td><td>{{$value->status}}</td><td><a href='/admin/project_crew/edit/{{$value->id}}' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/project_crew/delete/{{$value->id}}' class='btn btn-danger' data-original-title='' title=''>delete</a></td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection