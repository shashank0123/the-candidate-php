<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Finest Admin Panel for your dynamic website">
    <meta name="keywords" content="admin panel, Backstage Supporters, Dashboard,web app">
    <meta name="author" content="Shashank shekhar">
    <link rel="icon" href="/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
    <title>Admin Panel Powered By Backstage</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="/adminassets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="/adminassets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="/adminassets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="/adminassets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="/adminassets/css/feather-icon.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="/adminassets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="/adminassets/css/chartist.css">
    <link rel="stylesheet" type="text/css" href="/adminassets/css/date-picker.css">
    <!-- Plugins css Ends-->
    <link rel="stylesheet" type="text/css" href="/adminassets/css/datatables.css">
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="/adminassets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="/adminassets/css/style.css">
    <link id="color" rel="stylesheet" href="/adminassets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="/adminassets/css/responsive.css">
  </head>
  <body onload="startTime()">
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="loader-index"><span></span></div>
      <svg>
        <defs></defs>
        <filter id="goo">
          <fegaussianblur in="SourceGraphic" stddeviation="11" result="blur"></fegaussianblur>
          <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo">    </fecolormatrix>
        </filter>
      </svg>
    </div>
    <!-- Loader ends-->
    @include('admin.layouts.topbar')
      <!-- Page Body Start-->
      <div class="page-body-wrapper sidebar-icon">
        <!-- Page Sidebar Start-->
        <header class="main-nav">
          <div class="logo-wrapper"><a href="index.html"><img class="img-fluid" src="/adminassets/images/logo/logo.png" alt=""></a></div>
          <div class="logo-icon-wrapper"><a href="index.html"><img class="img-fluid" src="/adminassets/images/logo/logo-icon.png" alt=""></a></div>
          @include('admin.layouts.sidebar')
        </header>
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-lg-6">
                  <h3>
                     Dashboard</h3>
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/admin/dashboard"><i data-feather="home"></i></a></li>
                    <li class="breadcrumb-item">Dashboard</li>
                  </ol>
                </div>
                <div class="col-lg-6">
                  <!-- Bookmark Start-->
                  <div class="bookmark pull-right">
                    <ul>
                      <li><a href="#" data-container="body" data-toggle="popover" data-placement="top" title="" data-original-title="Chat"><i data-feather="message-square"></i></a></li>
                      <li><a href="#" data-container="body" data-toggle="popover" data-placement="top" title="" data-original-title="Icons"><i data-feather="command"></i></a></li>
                      <li><a href="#" data-container="body" data-toggle="popover" data-placement="top" title="" data-original-title="Learning"><i data-feather="layers"></i></a></li>
                      <li><a href="#"><i class="bookmark-search" data-feather="star"></i></a>
                        <form class="form-inline search-form">
                          <div class="form-group form-control-search">
                            <input type="text" placeholder="Search..">
                          </div>
                        </form>
                      </li>
                    </ul>
                  </div>
                  <!-- Bookmark Ends-->
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
        @yield('content')
        </div>
        </div>


         <!-- footer start-->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">Copyright 2018 © Backstage All rights reserved.</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0">Made with Love<i class="fa fa-heart font-secondary"></i><a href="http://backstagesupporters.com" target="_blank" rel="noopener noreferrer">Backstage</a> </p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- latest jquery-->
    <script src="/adminassets/js/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap js-->
    <script src="/adminassets/js/bootstrap/popper.min.js"></script>
    <script src="/adminassets/js/bootstrap/bootstrap.js"></script>
    <!-- feather icon js-->
    <script src="/adminassets/js/icons/feather-icon/feather.min.js"></script>
    <script src="/adminassets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="/adminassets/js/sidebar-menu.js"></script>
    <script src="/adminassets/js/config.html"></script>
    <!-- Plugins JS start-->
    <script src="/adminassets/js/chart/chartist/chartist.html"></script>
    <script src="/adminassets/js/chart/chartist/chartist-plugin-tooltip.html"></script>
    <script src="/adminassets/js/chart/knob/knob.min.js"></script>
    <script src="/adminassets/js/chart/knob/knob-chart.js"></script>
    <script src="/adminassets/js/chart/apex-chart/apex-chart.js"></script>
    <script src="/adminassets/js/chart/apex-chart/stock-prices.html"></script>
    <script src="/adminassets/js/notify/bootstrap-notify.min.js"></script>
    <script src="/adminassets/js/dashboard/default.js"></script>
    <script src="/adminassets/js/notify/index.js"></script>
    <script src="/adminassets/js/datepicker/date-picker/datepicker.js"></script>
    <script src="/adminassets/js/datepicker/date-picker/datepicker.en.js"></script>
    <script src="/adminassets/js/datepicker/date-picker/datepicker.custom.js"></script>
    <!-- Plugins JS start-->
    <script src="/adminassets/js/datatable/datatables/jquery.dataTables.min.js"></script>
    <script src="/adminassets/js/datatable/datatables/datatable.custom.js"></script>
    <script src="/adminassets/js/tooltip-init.html"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="/adminassets/js/script.html"></script>
    <script src="/adminassets/js/theme-customizer/customizer.js"></script>
    <!-- login js-->
    <!-- Plugin used-->
    <!--tinyMCE-->
    <script src="/adminassets/plugins/tinymce/jquery.tinymce.min.js"></script>
    <script src="/adminassets/plugins/tinymce/tinymce.min.js"></script>
    <script>
    function init_tinymce(selector, min_height) {
        var menu_bar = 'file edit view insert format tools table help';
        if (selector == '.tinyMCEQuiz') {
            menu_bar = false;
        }
        tinymce.init({
            selector: selector,
            min_height: min_height,
            valid_elements: '*[*]',
            relative_urls: false,
            remove_script_host: false,
            language: 'en',
            menubar: menu_bar,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code codesample fullscreen",
                "insertdatetime media table paste imagetools"
            ],
            toolbar: 'fullscreen code preview | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | numlist bullist | forecolor backcolor removeformat | image media link | outdent indent',
            content_css: ['/adminassets/plugins/tinymce/editor_content.css'],
        });
        tinymce.DOM.loadCSS('/adminassets/plugins/tinymce/editor_ui.css');
    }

    if ($('.tinyMCE').length > 0) {
        init_tinymce('.tinyMCE', 500);
    }
    if ($('.tinyMCEsmall').length > 0) {
        init_tinymce('.tinyMCEsmall', 300);
    }
    if ($('.tinyMCEQuiz').length > 0) {
        init_tinymce('.tinyMCEQuiz', 200);
    }

    $(document).on('click', '.btn_tinymce_add_media', function () {
        var editor_id = $(this).attr('data-editor-id');
        $("#" + editor_id + " [aria-label='Insert/edit media']").trigger("click");
    });
</script>

  </body>

</html>
