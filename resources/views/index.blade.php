@extends('layouts.app')
@section('styles')
<?php use App\Models\Slider;
$sliders = Slider::all();
?>
<style>
    .mySlides {display: none;}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1600px;
  position: relative;
  margin: auto;
  height:80vh;
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .text {font-size: 11px}
}
    </style>
@endsection
@section('contents')
 <div id="theme-page" class="master-holder clearfix" itemscope='"itemscope"' itemtype='"https://schema.org/Blog"'>
        <div class="master-holder-bg-holder">
          <div id="theme-page-bg" class="master-holder-bg js-el" data-mk-component="Parallax" data-parallax-config='{"speed" : 0.3 }' style="
                background-attachment: scroll;
                will-change: transform;
                transform: translateY(-12.3px) translateZ(0px);
                height: 2238.65px;
              "></div>
        </div>
        <div class="mk-main-wrapper-holder">
          <div id="mk-page-id-82" class="theme-page-wrapper mk-main-wrapper full-width-layout no-padding">
            <div class="theme-content no-padding" itemprop="mainEntityOfPage">
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-true attched-false js-master-row mk-full-content-true mk-in-viewport">
                <div class="vc_col-sm-12 wpb_column column_container ">
                    <div class="slideshow-container">

                        @foreach ($sliders as $slider)

                        <div class="mySlides fade">
                            <img src="<?php echo $slider->image?>" style="width:100%">
                        </div>

                        @endforeach


                    </div>
<br>

<div style="text-align:center">
    @foreach ($sliders as $slider)
  <span class="dot"></span>
    @endforeach
</div>
                </div>
              </div>


              {{-- trips
                tripcategory
                blogs --}}

                @if (count($tripcategory)>0)
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false js-master-row mk-grid">
                <div class="vc_col-sm-12 wpb_column column_container">
                  <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                      <div id="upcoming_community_trips" class="all_web_headings">
                        Trending Package
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-true attched-false vc_custom_1569595094188 js-master-row mk-full-content-true">
                <div class="vc_col-sm-12 wpb_column column_container ">
                  <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                      <div class="homepage_our_trips">


                        @foreach ($tripcategory as $item)

                        <a href="<?php echo str_replace(' ','-',$item->category_name).'-'.$item->id; ?>" class="homepage_our_trips_items">
                          <div class="homepage_our_trips_items_image " style="background-image:linear-gradient(
            to top,
            rgba(0, 0, 0, 0.7) 15%,
            rgba(0, 0, 0, 0) 30%
        ),
        url('<?php echo $item->category_image; ?>')">
                            <div class="homepage_our_trips_items_image_heading">
                              <span class="homepage_our_trips_items_image_heading_span">{{$item->category_name}}</span>
                            </div>
                            <div class="homepage_our_trips_items_image_costing">
                              <img src="/assets/images/extras/rupee.svg" style="height: 0.9em;" /><br />Starting @ INR {{$item->default_price}}/-
                            </div>
                            <div class="homepage_our_trips_items_image_duration">
                              <img src="/assets/images/extras/calendar.svg" style="height: 0.9em;" /><br />{{$item->default_days}}+ Days
                            </div>
                          </div>
                          <!--<div class="homepage_our_trips_items_description">Weeklong getaways from the monotony of Life and escape to serene destinations</div>-->
                        </a>
                        @endforeach



                      </div>
                    </div>
                  </div>
                </div>
              </div>

              @endif







              @if (count($trips) > 0)
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false js-master-row mk-grid">
                <div class="vc_col-sm-12 wpb_column column_container ">
                  <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                      <div id="booking_id_generator" style="display: none;">
                        0
                      </div>
                    </div>
                  </div>
                  <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                      <div class="all_web_headings" style="margin-bottom: -10px;">
                        Make your own plan
                      </div>
                    </div>
                  </div>
                  <div class="vc_custom_1570096376772">
                    <div id="text-block-7" class="mk-text-block">
                      <p style="text-align: center;">

                      </p>
                      <div class="clearboth"></div>
                    </div>
                  </div>
                  <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                      <div class="homepage_byog">

                        @foreach ($trips as $item)

                      <a href="/newtrip-{{$item->category_id}}/{{$item->trip_name}}-{{$item->id}}" class="homepage_byog_items homepage_byog_items_andaman_lazy" style="text-decoration-line: none; background-image: url('<?php echo $item->cover_image; ?>');" target="_blank">
                          <span class="homepage_byog_items_heading_span"><span class="homepage_byog_items_heading">{{$item->trip_name}}</span></span>
                        </a>
                        @endforeach
                      </div>
                    </div>
                  </div>
                  <div id="padding-8" class="mk-padding-divider clearfix"></div>
                </div>
              </div>
              @endif

              @if (count($blogs) > 0)
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-true attched-false js-master-row mk-full-content-true">
                <div class="vc_col-sm-12 wpb_column column_container ">
                  <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                      <div class="all_web_headings">
                        Blogs
                      </div>
                      <div class="blog_list_container">

                        @foreach ($blogs as $item)

                        <a href="/blogs" class="blog_list_item">
                          <div class="blog_list_item_text">
                            <span class="blog_list_item_title">{{$item->title}}</span>
                            <span class="blog_list_item_description"><img src="/assets/images/extras/clock.svg" style="height: 0.9em;" />&nbsp;5 minutes read</span>
                          </div>
                          <div class="blog_list_item_image blog_list_item_image_dussehra_lazy" style="background-image: url(<?php echo $item->featured_image?>);"></div>
                        </a>

                        @endforeach


                      </div>
                      <a href="blogs/" target="_blank">
                        <div class="any_button_div">
                          <span class="any_button">Read more</span>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              @endif


              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false js-master-row mk-grid">
                <div class="vc_col-sm-12 wpb_column column_container ">
                  <div class="wpb_raw_code wpb_raw_js">
                    <div class="wpb_wrapper">
                      <script type="text/javascript">
                        var back1 = document.querySelector(
                          ".homepage_our_trips_items_image_bangalore_lazy"
                        );
                        var back2 = document.querySelector(
                          ".homepage_our_trips_items_image_escapades_lazy"
                        );
                        var back3 = document.querySelector(
                          ".homepage_byog_items_andaman_lazy"
                        );
                        var back4 = document.querySelector(
                          ".homepage_byog_items_himachal_lazy"
                        );
                        var back5 = document.querySelector(
                          ".homepage_byog_items_kashmir_lazy"
                        );
                        var back6 = document.querySelector(
                          ".homepage_byog_items_kerala_lazy"
                        );
                        var back7 = document.querySelector(
                          ".homepage_byog_items_north_east_lazy"
                        );
                        var back8 = document.querySelector(
                          ".homepage_byog_items_uttarakhand_lazy"
                        );
                        var back9 = document.querySelector(
                          ".blog_list_item_image_dussehra_lazy"
                        );
                        var back10 = document.querySelector(
                          ".blog_list_item_image_bangalore_lazy"
                        );
                        var back11 = document.querySelector(
                          ".blog_list_item_image_bhutan_lazy"
                        );
                        var back12 = document.querySelector(
                          ".blog_list_item_image_meghalaya_lazy"
                        );
                        var back13 = document.querySelector(
                          ".blog_list_item_image_chadar_lazy"
                        );
                        var back14 = document.querySelector(
                          ".blog_list_item_image_sikkim_lazy"
                        );
                        window.onscroll = function() {
                          if (
                            document.body.scrollTop > 350 ||
                            document.documentElement.scrollTop > 350
                          ) {
                            back1.style.backgroundImage =
                              "linear-gradient(to top, rgba(0,0,0,0.7) 15%, rgba(0,0,0,0) 30%), url('wp-content/uploads/2019/09/wanderon-gokarna-sunset.jpg')";
                            back2.style.backgroundImage =
                              "linear-gradient(to top, rgba(0,0,0,0.7) 15%, rgba(0,0,0,0) 30%), url('wp-content/uploads/2019/07/himachal.jpg')";
                            back3.style.backgroundImage =
                              "url('wp-content/uploads/2019/03/andamanescapade.jpg')";
                            back4.style.backgroundImage =
                              "url('wp-content/uploads/2019/03/tirthan-valley-hidden-gem-of-himachal-1-13.jpg')";
                            back5.style.backgroundImage =
                              "url('wp-content/uploads/2019/03/kashmirdelight.jpg')";
                            back6.style.backgroundImage =
                              "url('wp-content/uploads/2019/03/gemsofkeralawithalleppey.jpg')";
                            back7.style.backgroundImage =
                              "url('wp-content/uploads/2019/03/meghalayawithkaziranga.jpg')";
                            back8.style.backgroundImage =
                              "url('wp-content/uploads/2019/03/magnificientuttarakhand.jpg')";
                            back9.style.backgroundImage =
                              "url('wp-content/uploads/2019/10/places-to-visit-on-this-long-dussehra-weekend.jpg')";
                            back10.style.backgroundImage =
                              "url('wp-content/uploads/2019/10/goa.jpg')";
                            back11.style.backgroundImage =
                              "url('wp-content/uploads/2019/10/learn-to-laugh-even-at-yourself.jpg')";
                            back12.style.backgroundImage =
                              "url('wp-content/uploads/2019/10/falls-in-meghalaya.jpg')";
                            back13.style.backgroundImage =
                              "url('wp-content/uploads/2019/10/witness-a-whole-new-world-in-chadar-trek-1.jpg')";
                            back14.style.backgroundImage =
                              "url('wp-content/uploads/2019/10/sikkim-travel-package.jpg')";
                          }
                        };
                      </script>
                    </div>
                  </div>
                </div>
              </div>


              @if (isset($customisedtrip))
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false js-master-row mk-grid">
                <div class="vc_col-sm-12 wpb_column column_container ">
                  <div id="padding-12" class="mk-padding-divider clearfix"></div>
                  <div class="vc_custom_1573641186536">
                    <div id="text-block-13" class="mk-text-block">
                      <h1 class="mkdf-section-title mkdf-section-title-large" style="text-align: center; color: black;">
                        We take care of Everything
                      </h1>
                      <div class="mkdf-section-subtitle-holder mkdf-section-subtitle-center">
                        <p style="text-align: center;">
                          Travel along the picturesque destinations, indulge
                          in thrilling activities and enjoy the charm of
                          nature without worrying about the minutest of issues
                          regarding your travel plans! Our highly skilled and
                          experienced group captains arrange all that you
                          need, while your eyes and body take a treat from
                          nature. Bring together your friends, family or
                          colleagues and be ready to collect innumerable
                          moments that always have an element of serendipity
                          on our amazing group trips!
                        </p>
                      </div>
                      <div class="clearboth"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false js-master-row mk-grid">
    <div class="vc_col-sm-4 vc_col-lg-4 vc_col-md-4 wpb_column column_container _ height-full">
        <div class="vc_custom_1553679745464">
            <div id="text-block-15" class="mk-text-block">
                <h2 class="mkdf-section-title mkdf-section-title-large" style="text-align: center; color: black;">
                    Family Trip
                </h2>
                <div class="clearboth"></div>
            </div>
        </div>
        <div class="wpb_single_image wpb_content_element vc_align_center">
            <figure class="wpb_wrapper vc_figure">
                <a href="/enquiry-page.php" target="_self" class="vc_single_image-wrapper vc_box_border_grey"><img width="600" height="342" src="wp-content/uploads/2020/01/family-trip.jpg" class="vc_single_image-img attachment-full" alt="" srcset="
                              wp-content/uploads/2020/01/family-trip.jpg         600w,
                              wp-content/uploads/2020/01/family-trip-300x171.jpg 300w
                            " sizes="(max-width: 600px) 100vw, 600px" itemprop="image" /></a>
            </figure>
        </div>
        <div class="vc_custom_1553679737105">
            <div id="text-block-16" class="mk-text-block">
                <div class="mkdf-section-subtitle-holder mkdf-section-subtitle-center">
                    <p class="mkdf-section-subtitle">
                        Grandparents, parents, siblings, uncles, aunts-
                        everyone has a unique picture of their perfect trip
                        which surely makes planning a family trip one hell
                        of a nightmare. With our expertise, all you need is
                        to give us are your dates, and we’ll figure out the
                        best plan for your family!
                    </p>
                </div>
                <div class="clearboth"></div>
            </div>
        </div>
    </div>
    <div class="vc_col-sm-4 vc_col-lg-4 vc_col-md-4 wpb_column column_container _ height-full">
        <div class="vc_custom_1553679762427">
            <div id="text-block-18" class="mk-text-block">
                <h2 class="mkdf-section-title mkdf-section-title-large" style="text-align: center; color: black;">
                    College Trip
                </h2>
                <div class="clearboth"></div>
            </div>
        </div>
        <div class="wpb_single_image wpb_content_element vc_align_center">
            <figure class="wpb_wrapper vc_figure">
                <a href="/enquiry-page.php" target="_self" class="vc_single_image-wrapper vc_box_border_grey"><img width="600" height="342" src="wp-content/uploads/2020/01/college-trip-2.jpg" class="vc_single_image-img attachment-full" alt="" srcset="
                              wp-content/uploads/2020/01/college-trip-2.jpg         600w,
                              wp-content/uploads/2020/01/college-trip-2-300x171.jpg 300w
                            " sizes="(max-width: 600px) 100vw, 600px" itemprop="image" /></a>
            </figure>
        </div>
        <div class="vc_custom_1553679756270">
            <div id="text-block-19" class="mk-text-block">
                <div class="mkdf-section-subtitle-holder mkdf-section-subtitle-center">
                    <p class="mkdf-section-subtitle">
                        Dilemmas in deciding where to go, get the best
                        travel memories and keep your bank accounts, alive
                        at the same time? Keep your hassles aside and create
                        the finest travel memories with your college buddies
                        through our specially curated trips for college
                        students.
                    </p>
                </div>
                <div class="clearboth"></div>
            </div>
        </div>
    </div>
    <div class="vc_col-sm-4 vc_col-lg-4 vc_col-md-4 wpb_column column_container _ height-full">
        <div class="vc_custom_1553679778045">
            <div id="text-block-21" class="mk-text-block">
                <h2 class="mkdf-section-title mkdf-section-title-large" style="text-align: center; color: black;">
                    Corporate Trip
                </h2>
                <div class="clearboth"></div>
            </div>
        </div>
        <div class="wpb_single_image wpb_content_element vc_align_center">
            <figure class="wpb_wrapper vc_figure">
                <a href="/enquiry-page.php" target="_self" class="vc_single_image-wrapper vc_box_border_grey"><img width="600" height="342" src="/assets/images/trip/corporate-trip.jpg" class="vc_single_image-img attachment-full" alt="" srcset="
                              /assets/images/trip/corporate-trip.jpg" itemprop="image" /></a>
            </figure>
        </div>
        <div class="vc_custom_1553679771785">
            <div id="text-block-22" class="mk-text-block">
                <div class="mkdf-section-subtitle-holder mkdf-section-subtitle-center">
                    <p class="mkdf-section-subtitle">
                        Travelling is scientifically proven to make people
                        more productive. So it’s High time that you gather
                        your work colleagues&nbsp;and hit the divinely
                        soothing destinations to rejuvenate yourselves with
                        the best of nature with WanderOn’s customized group
                        trips.
                    </p>
                </div>
                <div class="clearboth"></div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="clearboth"></div>
              <div class="clearboth"></div>
            </div>
            <div class="clearboth"></div>
          </div>
        </div>
        <div class="racb_button">Request a Call Back</div>
      </div>
      <section id="mk-footer-unfold-spacer"></section>
      <script>
var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 3000); // Change image every 2 seconds
}
</script>
@endsection
