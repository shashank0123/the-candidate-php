<section id="mk-footer" class="" role="contentinfo" itemscope="itemscope" itemtype="https://schema.org/WPFooter">
    <div class="footer-wrapper fullwidth-footer">
        <div class="mk-padding-wrapper">
            <div class="mk-col-1-4">
                <section id="text-16" class="widget widget_text">
                    <div class="widgettitle">About Us</div>
                    <div class="textwidget">
                        <?php echo $about_us?>
                    </div>
                </section>
            </div>
            <div class="mk-col-1-4">
                <section id="custom_html-5" class="widget_text widget widget_custom_html">
                    <div class="textwidget custom-html-widget">
                        <p style="
                        font-size: 0.8em;
                        font-family: open sans;
                        font-weight: 400;
                      ">
                            <a style="color: #fff; font-weight: 600; font-size: 1.2em;" onmouseover="this.style.color='#FFF'" onmouseout="this.style.color='#FFF'" href="https://www.wanderon.in/blogs">BLOG POSTS</a><br /><br />
                            @foreach ($footerblog as $blog)

                            <a style="color: #888;" onmouseover="this.style.color='#FFF'" onmouseout="this.style.color='#888'" href="/blogs/{{$blog->title}}-{{$blog->id}}">{{$blog->title}}</a><br />
                            @endforeach

                        </p>

                        <p style="
                        font-size: 0.8em;
                        font-family: open sans;
                        font-weight: 400;
                        ">
                            <a style="color: #fff; font-weight: 600; font-size: 1.2em;" onmouseover="this.style.color='#FFF'" onmouseout="this.style.color='#FFF'">Spiti Valley</a><br /><br />
                            @foreach ($footertrip1 as $trip)

                                <a style="color: #888;" onmouseover="this.style.color='#FFF'" onmouseout="this.style.color='#888'" href="{{'new-trip'}}-{{$trip->category_id}}/{{$trip->trip_name}}-{{$trip->id}}">{{$trip->trip_name}}</a><br />
                            @endforeach
                        </p>
                    </div>
                </section>
            </div>
            <div class="mk-col-1-4">
                <section id="custom_html-4" class="widget_text widget widget_custom_html">
                    <div class="textwidget custom-html-widget">
                        <p style="
                        font-size: 0.8em;
                        font-family: open sans;
                        font-weight: 400;
                      ">
                            <a style="color: #fff; font-weight: 600; font-size: 1.2em;" onmouseover="this.style.color='#FFF'" onmouseout="this.style.color='#FFF'">WEEKEND TRIPS</a><br /><br />
                           @foreach ($footertrip2 as $trip)

                                <a style="color: #888;" onmouseover="this.style.color='#FFF'" onmouseout="this.style.color='#888'" href="{{'new-trip'}}-{{$trip->category_id}}/{{$trip->trip_name}}-{{$trip->id}}">{{$trip->trip_name}}</a><br />
                            @endforeach
                        </p>

                        <p style="
                        font-size: 0.8em;
                        font-family: open sans;
                        font-weight: 400;
                      ">
                            <a style="color: #fff; font-weight: 600; font-size: 1.2em;" onmouseover="this.style.color='#FFF'" onmouseout="this.style.color='#FFF'">4 NIGHTS/5 DAYS TRIPS</a><br /><br />
                            @foreach ($footertrip3 as $trip)

                                <a style="color: #888;" onmouseover="this.style.color='#FFF'" onmouseout="this.style.color='#888'" href="{{'new-trip'}}-{{$trip->category_id}}/{{$trip->trip_name}}-{{$trip->id}}">{{$trip->trip_name}}</a><br />
                            @endforeach
                        </p>
                    </div>
                </section>
            </div>
            <div class="mk-col-1-4">
                <section id="contact_info-2" class="widget widget_contact_info">
                    <div class="widgettitle">Address</div>
                    <ul itemscope="itemscope" itemtype="https://schema.org/Person">
                       <?php echo $address?>
                    </ul>
                </section>

            </div>
            <div class="clearboth"></div>
        </div>
    </div>
    <div id="sub-footer">
        <div class="fullwidth-footer">
            <span class="mk-footer-copyright">©Copyright 2020 by CaptureATrip. All Rights Reserved.
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="/privacy-policy">Privacy Policy</a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="/cancellation">Cancellation Policy</a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="/terms-and-conditions">TnC</a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="/disclaimer/">Disclaimer</a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div class="footerExtraHeight"></div>
            </span>
        </div>
        <div class="clearboth"></div>
    </div>
</section>
