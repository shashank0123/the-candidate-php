<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('updatewebsite', 'WebsiteUpdateController@updateSite');


Route::get('/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/login', 'Admin\Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/home', 'Admin\AdminController@index')->name('admin.home');


Route::group(['middleware' => ['auth:admin']], function () {
    Route::get('/dashboard', 'Admin\DashboardController@getDashboard');
Route::get('/slider','Admin\SliderController@index');
    Route::get('slider/add','Admin\SliderController@add');
    Route::post('slider/add','Admin\SliderController@store');
    Route::get('slider/edit/{id}','Admin\SliderController@edit');
    Route::post('slider/edit/{id}','Admin\SliderController@update');
    Route::get('slider/delete/{id}','Admin\SliderController@delete');  
Route::get('/sitesetting','Admin\SiteSettingController@index');
    Route::get('sitesetting/add','Admin\SiteSettingController@add');
    Route::post('sitesetting/add','Admin\SiteSettingController@store');
    Route::get('sitesetting/edit/{id}','Admin\SiteSettingController@edit');
    Route::post('sitesetting/edit/{id}','Admin\SiteSettingController@update');
    Route::get('sitesetting/delete/{id}','Admin\SiteSettingController@delete');  
Route::get('/sitemenu','Admin\SiteMenuController@index');
    Route::get('sitemenu/add','Admin\SiteMenuController@add');
    Route::post('sitemenu/add','Admin\SiteMenuController@store');
    Route::get('sitemenu/edit/{id}','Admin\SiteMenuController@edit');
    Route::post('sitemenu/edit/{id}','Admin\SiteMenuController@update');
    Route::get('sitemenu/delete/{id}','Admin\SiteMenuController@delete');  
Route::get('/staticpage','Admin\StaticPageController@index');
    Route::get('staticpage/add','Admin\StaticPageController@add');
    Route::post('staticpage/add','Admin\StaticPageController@store');
    Route::get('staticpage/edit/{id}','Admin\StaticPageController@edit');
    Route::post('staticpage/edit/{id}','Admin\StaticPageController@update');
    Route::get('staticpage/delete/{id}','Admin\StaticPageController@delete');  
Route::get('/album','Admin\AlbumController@index');
    Route::get('album/add','Admin\AlbumController@add');
    Route::post('album/add','Admin\AlbumController@store');
    Route::get('album/edit/{id}','Admin\AlbumController@edit');
    Route::post('album/edit/{id}','Admin\AlbumController@update');
    Route::get('album/delete/{id}','Admin\AlbumController@delete');  
Route::get('/album_image','Admin\AlbumImageController@index');
    Route::get('album_image/add','Admin\AlbumImageController@add');
    Route::post('album_image/add','Admin\AlbumImageController@store');
    Route::get('album_image/edit/{id}','Admin\AlbumImageController@edit');
    Route::post('album_image/edit/{id}','Admin\AlbumImageController@update');
    Route::get('album_image/delete/{id}','Admin\AlbumImageController@delete');  
Route::get('/profile','Admin\ProfileController@index');
    Route::get('profile/add','Admin\ProfileController@add');
    Route::post('profile/add','Admin\ProfileController@store');
    Route::get('profile/edit/{id}','Admin\ProfileController@edit');
    Route::post('profile/edit/{id}','Admin\ProfileController@update');
    Route::get('profile/delete/{id}','Admin\ProfileController@delete');  
Route::get('/user_assignmnent','Admin\UserAssignmnentController@index');
    Route::get('user_assignmnent/add','Admin\UserAssignmnentController@add');
    Route::post('user_assignmnent/add','Admin\UserAssignmnentController@store');
    Route::get('user_assignmnent/edit/{id}','Admin\UserAssignmnentController@edit');
    Route::post('user_assignmnent/edit/{id}','Admin\UserAssignmnentController@update');
    Route::get('user_assignmnent/delete/{id}','Admin\UserAssignmnentController@delete');  
Route::get('/user_project','Admin\UserProjectController@index');
    Route::get('user_project/add','Admin\UserProjectController@add');
    Route::post('user_project/add','Admin\UserProjectController@store');
    Route::get('user_project/edit/{id}','Admin\UserProjectController@edit');
    Route::post('user_project/edit/{id}','Admin\UserProjectController@update');
    Route::get('user_project/delete/{id}','Admin\UserProjectController@delete');  
Route::get('/user_social_account','Admin\UserSocialAccountController@index');
    Route::get('user_social_account/add','Admin\UserSocialAccountController@add');
    Route::post('user_social_account/add','Admin\UserSocialAccountController@store');
    Route::get('user_social_account/edit/{id}','Admin\UserSocialAccountController@edit');
    Route::post('user_social_account/edit/{id}','Admin\UserSocialAccountController@update');
    Route::get('user_social_account/delete/{id}','Admin\UserSocialAccountController@delete');  
Route::get('/project','Admin\ProjectController@index');
    Route::get('project/add','Admin\ProjectController@add');
    Route::post('project/add','Admin\ProjectController@store');
    Route::get('project/edit/{id}','Admin\ProjectController@edit');
    Route::post('project/edit/{id}','Admin\ProjectController@update');
    Route::get('project/delete/{id}','Admin\ProjectController@delete');  
Route::get('/project_crew','Admin\ProjectCrewController@index');
    Route::get('project_crew/add','Admin\ProjectCrewController@add');
    Route::post('project_crew/add','Admin\ProjectCrewController@store');
    Route::get('project_crew/edit/{id}','Admin\ProjectCrewController@edit');
    Route::post('project_crew/edit/{id}','Admin\ProjectCrewController@update');
    Route::get('project_crew/delete/{id}','Admin\ProjectCrewController@delete');  
Route::get('/project_script','Admin\ProjectScriptController@index');
    Route::get('project_script/add','Admin\ProjectScriptController@add');
    Route::post('project_script/add','Admin\ProjectScriptController@store');
    Route::get('project_script/edit/{id}','Admin\ProjectScriptController@edit');
    Route::post('project_script/edit/{id}','Admin\ProjectScriptController@update');
    Route::get('project_script/delete/{id}','Admin\ProjectScriptController@delete');  
Route::get('/script_scene','Admin\ScriptSceneController@index');
    Route::get('script_scene/add','Admin\ScriptSceneController@add');
    Route::post('script_scene/add','Admin\ScriptSceneController@store');
    Route::get('script_scene/edit/{id}','Admin\ScriptSceneController@edit');
    Route::post('script_scene/edit/{id}','Admin\ScriptSceneController@update');
    Route::get('script_scene/delete/{id}','Admin\ScriptSceneController@delete');  
Route::get('/project_lead','Admin\ProjectLeadController@index');
    Route::get('project_lead/add','Admin\ProjectLeadController@add');
    Route::post('project_lead/add','Admin\ProjectLeadController@store');
    Route::get('project_lead/edit/{id}','Admin\ProjectLeadController@edit');
    Route::post('project_lead/edit/{id}','Admin\ProjectLeadController@update');
    Route::get('project_lead/delete/{id}','Admin\ProjectLeadController@delete');  
Route::get('/project_task','Admin\ProjectTaskController@index');
    Route::get('project_task/add','Admin\ProjectTaskController@add');
    Route::post('project_task/add','Admin\ProjectTaskController@store');
    Route::get('project_task/edit/{id}','Admin\ProjectTaskController@edit');
    Route::post('project_task/edit/{id}','Admin\ProjectTaskController@update');
    Route::get('project_task/delete/{id}','Admin\ProjectTaskController@delete');  
Route::get('/project_log','Admin\ProjectLogController@index');
    Route::get('project_log/add','Admin\ProjectLogController@add');
    Route::post('project_log/add','Admin\ProjectLogController@store');
    Route::get('project_log/edit/{id}','Admin\ProjectLogController@edit');
    Route::post('project_log/edit/{id}','Admin\ProjectLogController@update');
    Route::get('project_log/delete/{id}','Admin\ProjectLogController@delete');  
Route::get('/assignment','Admin\AssignmentController@index');
    Route::get('assignment/add','Admin\AssignmentController@add');
    Route::post('assignment/add','Admin\AssignmentController@store');
    Route::get('assignment/edit/{id}','Admin\AssignmentController@edit');
    Route::post('assignment/edit/{id}','Admin\AssignmentController@update');
    Route::get('assignment/delete/{id}','Admin\AssignmentController@delete');  
Route::get('/assignment_lead','Admin\AssignmentLeadController@index');
    Route::get('assignment_lead/add','Admin\AssignmentLeadController@add');
    Route::post('assignment_lead/add','Admin\AssignmentLeadController@store');
    Route::get('assignment_lead/edit/{id}','Admin\AssignmentLeadController@edit');
    Route::post('assignment_lead/edit/{id}','Admin\AssignmentLeadController@update');
    Route::get('assignment_lead/delete/{id}','Admin\AssignmentLeadController@delete');  
Route::get('/assignment_assigned','Admin\AssignmentAssignedController@index');
    Route::get('assignment_assigned/add','Admin\AssignmentAssignedController@add');
    Route::post('assignment_assigned/add','Admin\AssignmentAssignedController@store');
    Route::get('assignment_assigned/edit/{id}','Admin\AssignmentAssignedController@edit');
    Route::post('assignment_assigned/edit/{id}','Admin\AssignmentAssignedController@update');
    Route::get('assignment_assigned/delete/{id}','Admin\AssignmentAssignedController@delete');  
Route::get('/country','Admin\CountryController@index');
    Route::get('country/add','Admin\CountryController@add');
    Route::post('country/add','Admin\CountryController@store');
    Route::get('country/edit/{id}','Admin\CountryController@edit');
    Route::post('country/edit/{id}','Admin\CountryController@update');
    Route::get('country/delete/{id}','Admin\CountryController@delete');  
Route::get('/state','Admin\StateController@index');
    Route::get('state/add','Admin\StateController@add');
    Route::post('state/add','Admin\StateController@store');
    Route::get('state/edit/{id}','Admin\StateController@edit');
    Route::post('state/edit/{id}','Admin\StateController@update');
    Route::get('state/delete/{id}','Admin\StateController@delete');  
Route::get('/profession','Admin\ProfessionController@index');
    Route::get('profession/add','Admin\ProfessionController@add');
    Route::post('profession/add','Admin\ProfessionController@store');
    Route::get('profession/edit/{id}','Admin\ProfessionController@edit');
    Route::post('profession/edit/{id}','Admin\ProfessionController@update');
    Route::get('profession/delete/{id}','Admin\ProfessionController@delete');  
Route::get('/social_account','Admin\SocialAccountController@index');
    Route::get('social_account/add','Admin\SocialAccountController@add');
    Route::post('social_account/add','Admin\SocialAccountController@store');
    Route::get('social_account/edit/{id}','Admin\SocialAccountController@edit');
    Route::post('social_account/edit/{id}','Admin\SocialAccountController@update');
    Route::get('social_account/delete/{id}','Admin\SocialAccountController@delete');  
//{Route to be added here from automation}
});
