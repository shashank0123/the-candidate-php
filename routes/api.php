<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/auth/register', 'AuthApiController@register');
Route::get('profession_list', 'SupportDataController@profession_list');
Route::get('looking_for_list', 'SupportDataController@profession_list');
Route::get('get_tnc', 'SupportDataController@getTnC');

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthApiController@login');
    Route::post('logout', 'AuthApiController@logout');
    Route::post('refresh', 'AuthApiController@refresh');
    Route::post('me', 'AuthApiController@me');

    //{Route to be added here from automation}

});

Route::group([
    'middleware' => ['auth:api', 'api']
], function ($router) {
    Route::get('profile', 'ProfileController@userProfile');
    Route::post('profile', 'ProfileController@saveProfile');
    Route::post('update_profile_pic', 'ProfileController@saveProfilePic');
    Route::get('save_tnc', 'SupportDataController@getTnC');
    Route::post('submit_profile', 'ProfileController@submit_profile');
    Route::post('upload_image', 'ProfileController@upload_image');

    //{Route to be added here from automation}

});


