<?php

use App\Http\Controllers\pageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'pageController@home');
Route::get('/privacy-policy', 'pageController@privacypolicy');
Route::get('/cancellation', 'pageController@cancellation');
Route::get('/terms-and-conditions', 'pageController@termsandconditions');
Route::get('/disclaimer', 'pageController@disclaimer');

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
